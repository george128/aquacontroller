#!/usr/bin/perl -w

print $#ARGV;

if($#ARGV < 1) {
    print "USE: cmd_escape.pl <infile> <outfile>\n";
    exit 10;
}

$infile=$ARGV[0];
$outfile=$ARGV[1];

open INFH, $infile or die "Can't open $infile for read!";
open OUTFH, ">$outfile" or die "Can't open $outfile for write!";
while($line=<INFH>) {
    print $line;
    if($line=~/(.*\s)\(([^\s|^\)]+)\)(.*)/) {
        print OUTFH "$1\\($2\\)$3\n";
    } else {
        print OUTFH "$line";
    }
}

close OUTFH;
close FH;
