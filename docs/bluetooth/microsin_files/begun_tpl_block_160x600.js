Begun.Autocontext.Customization.setTpls({
	"begun_html_tpls": [
		{"block_160x600": '\
<span class="begun_adv_span">\
<div id="{{begun_warn_id}}" class="begun_adv begun_adv_fix begun_adv_fix_ver {{fix_layout}}"{{block_hover}}>\
<div class="begun_adv_common {{block_scroll_class}} banners_count_{{banners_count}} {{extended_block_class}}" id="{{scroll_div_id}}">\
<table class="begun_adv_table {{css_thumbnails}}" id="{{scroll_table_id}}">\
{{banners}}\
</table>\
</div>\
<div class="begun_adv_sys_logo" style="display:{{logo_display}}"><div><a href="{{begun_url}}" target="_blank" class="snap_noshots">begun</a></div></div>\
{{block_warn}}\
</div>\
</span>\
'},
		{'banner_160x600': '\
<tr>\
<td class="begun_adv_cell" title="{{fullDomain}}" onclick="{{onclick}}" _url="{{url}}" _banner_id="{{banner_id}}">\
{{thumb}}\
<div class="begun_adv_block {{css_favicon}}" {{favicon}} title="{{fullDomain}}">\
<div class="begun_adv_title">{{cross}}<a class="snap_noshots" target="_blank" href="{{url}}" onmouseover="status=\'{{status}}\';return true" onmouseout="status=\'\';return true" title="{{fullDomain}}" {{favicon}}{{styleTitle}}>{{title}}</a>{{bnnr_warn}}</div>\
<div class="begun_adv_text"><a class="snap_noshots" target="_blank" href="{{url}}" onmouseover="status=\'{{status}}\';return true" onmouseout="status=\'\';return true" title="{{fullDomain}}"{{styleText}}>{{descr}}</a></div>\
<div class="begun_adv_contact"{{styleContact}}>{{contact}}</div>\
</div>\
</td>\
</tr>\
'},
		{'banner_160x600_rich': '\
<tr>\
<td class="begun_adv_cell begun_adv_rich" onclick="{{onclick}}" _url="{{url}}" _banner_id="{{banner_id}}">\
{{picture}}\
<div class="begun_adv_block {{css_favicon}}" {{favicon}} title="{{fullDomain}}">\
<div class="begun_adv_title"><a class="snap_noshots" target="_blank" href="{{url}}" onmouseover="status=\'{{status}}\';return true" onmouseout="status=\'\';return true" title="{{fullDomain}}"{{styleTitle}}>{{title}}</a></div>\
<div class="begun_adv_text"><a class="snap_noshots" target="_blank" href="{{url}}" onmouseover="status=\'{{status}}\';return true" onmouseout="status=\'\';return true" title="{{fullDomain}}"{{styleText}}>{{descr}}</a></div>\
<div class="begun_adv_contact"{{styleContact}}>{{contact}}</div>\
</div>\
</td>\
</tr>\
'}
	],
	"begun_css_tpls": [
		{"block_160x600": '\
#begun_block_{{block_id}} .begun_adv {\
	width: 160px !important;\
	height: 600px !important;\
}\
#begun_block_{{block_id}} .begun_adv.begun_fix_layout {\
	width: 158px !important;\
	height: 598px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_common {\
	height: 580px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_common.begun_extended_block {\
	height: 100% !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_common table {\
	height: 100% !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_block {\
	overflow: hidden !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .begun_adv_text * {\
	font-size: 13px !important;\
	line-height: 14px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .begun_adv_title * {\
	font-size: 14px !important;\
	line-height: 17px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_fav {\
    margin-left: -4px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_fav .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .begun_adv_fav .begun_adv_text * {\
	font-size: 11px !important;\
	line-height: 13px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_fav .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .begun_adv_fav .begun_adv_title * {\
	font-size: 12px !important;\
	line-height: 14px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_rich .begun_adv_image {\
	top: 0 !important;\
	float: none !important;\
	margin-bottom: 5px !important\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb .begun_thumb {\
	float: none !important;\
	margin: 5px 0 !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb .begun_thumb img {\
	float:none !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb .begun_adv_block,\
#begun_block_{{block_id}} .begun_adv .begun_adv_rich .begun_adv_block {\
	margin-left:0 !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_3 .begun_adv_cell,\
#begun_block_{{block_id}} .begun_adv .banners_count_3 .begun_adv_cell *,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_cell,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_cell * {\
	font-size:13px !important;\
	line-height:16px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_3 .begun_adv_phone,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_phone {\
	margin-top:4px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_3 .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .banners_count_3 .begun_adv_text *,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_text * {\
	font-size: 14px !important;\
	line-height: 17px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_3 .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .banners_count_3 .begun_adv_title *,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_title * {\
	font-size: 15px !important;\
	line-height: 18px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_cell,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_cell * {\
	font-size:15px !important;\
	line-height:18px !important;\
	text-align:center !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_phone {\
	margin-top:5px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_text * {\
	font-size: 16px !important;\
	line-height: 20px !important;\
	text-align: center !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_title * {\
	font-size: 18px !important;\
	line-height: 21px !important;\
	text-align: center !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb,\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb *,\
#begun_block_{{block_id}} .begun_adv .begun_adv_rich,\
#begun_block_{{block_id}} .begun_adv .begun_adv_rich * {\
	font-size:11px !important;\
	line-height:12px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb .begun_adv_text *,\
#begun_block_{{block_id}} .begun_adv .begun_adv_rich .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .begun_adv_rich .begun_adv_text * {\
	font-size:12px !important;\
	line-height:13px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .begun_adv_thumb .begun_adv_title *,\
#begun_block_{{block_id}} .begun_adv .begun_adv_rich .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .begun_adv_rich .begun_adv_title * {\
	font-size:13px !important;\
	line-height:14px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_thumb,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_thumb *,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_rich ,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_rich * {\
	font-size:13px !important;\
	line-height:14px !important;\
    text-align: left !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_thumb .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_thumb .begun_adv_text *,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_rich .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_rich .begun_adv_text * {\
	font-size: 15px !important;\
	line-height: 18px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_thumb .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_thumb .begun_adv_title *,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_rich .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .banners_count_1 .begun_adv_rich .begun_adv_title * {\
	font-size: 16px !important;\
	line-height: 20px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_thumb,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_thumb *,\
#begun_block_{{block_id}}.begun_auto_rich .begun_adv .banners_count_2 .begun_adv_rich ,\
#begun_block_{{block_id}}.begun_auto_rich .begun_adv .banners_count_2 .begun_adv_rich * {\
	font-size:12px !important;\
	line-height:13px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_thumb .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_thumb .begun_adv_text *,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_rich .begun_adv_text,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_rich .begun_adv_text *,\
#begun_block_{{block_id}}.begun_auto_rich .begun_adv .banners_count_2 .begun_adv_rich .begun_adv_text,\
#begun_block_{{block_id}}.begun_auto_rich .begun_adv .banners_count_2 .begun_adv_rich .begun_adv_text * {\
	font-size: 14px !important;\
	line-height: 17px !important;\
}\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_thumb .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_thumb .begun_adv_title *,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_rich .begun_adv_title,\
#begun_block_{{block_id}} .begun_adv .banners_count_2 .begun_adv_rich .begun_adv_title *,\
#begun_block_{{block_id}}.begun_auto_rich .begun_adv .banners_count_2 .begun_adv_title ,\
#begun_block_{{block_id}}.begun_auto_rich .begun_adv .banners_count_2 .begun_adv_title * {\
	font-size: 15px !important;\
	line-height: 18px !important;\
}\
#begun_block_{{block_id}} #begun_warn_{{block_id}}.begun_adv .begun_adv_common {\
	height: 516px !important;\
}\
#begun_block_{{block_id}} #begun_warn_{{block_id}}.begun_adv .begun_adv_common.begun_extended_block {\
	height: 534px !important;\
}\
#begun_block_{{block_id}} #begun_warn_{{block_id}}.begun_adv .begun_adv_text,\
#begun_block_{{block_id}} #begun_warn_{{block_id}}.begun_adv .begun_adv_text * {\
	font-size: 12px !important;\
	line-height: 13px !important;\
}\
#begun_block_{{block_id}} #begun_warn_{{block_id}}.begun_adv .begun_adv_title,\
#begun_block_{{block_id}} #begun_warn_{{block_id}}.begun_adv .begun_adv_title * {\
	font-size: 12px !important;\
	line-height: 13px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_warn_message {\
	padding: 8px 9px 0 17px !important;\
	height: 56px !important;\
	_height: 66px !important;\
}\
#begun_block_{{block_id}} .begun_adv.begun_fix_layout .begun_warn_message {\
	height: 56px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_warn_alco {\
	font-size: 13px !important;\
	line-height: 16px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_warn_tobacco {\
	font-size: 13px !important;\
	line-height: 16px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_warn_medicine {\
	font-size: 10px !important;\
	line-height: 12px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_warn_abortion {\
	line-height: 12px !important;\
}\
#begun_block_{{block_id}} .begun_adv .begun_warn_message span.begun_warn_asterisk {\
	left:7px !important;\
	top:7px !important;\
}\
\
'}
	]
});
/*$LastChangedRevision: 49966 $*/
Begun.Autocontext.tplLoaded("begun_tpl_block_160x600");
