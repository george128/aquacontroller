/**
 * 
 */
package org.aquacontroller.jioemul;

import java.io.IOException;

import javax.swing.JFrame;

/**
 * @author george
 *
 */
public class Main {

    private static final int DISPLAY_SERVER_PORT = 2345;
    
	public Main(String args[]) {
		int listenPort = DISPLAY_SERVER_PORT;
		JFrame frame = new JFrame("Java IO Emulator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MainContainer mainContainer = new MainContainer();
		IDisplay display = mainContainer.getDisplay();
		ITrigger trigger = mainContainer.getTrigger();
		
		if (args.length > 1 && "-l".equals(args[0])) {
			listenPort = Integer.parseInt(args[1]);
		}
		
		try {
			DisplayServer dispServ = new DisplayServer(listenPort, display, trigger);
			mainContainer.setButtonHandler(dispServ);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		frame.getContentPane().add(mainContainer);
		frame.pack();
		frame.setVisible(true);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Main(args);
	}

}
