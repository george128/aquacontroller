package org.aquacontroller.jioemul;

public interface IDisplay {
	public void setPage(int controller, int page);
	public void setX(int controller, int x);
	public void setData(int controller, byte data);
	public byte getData(int controller);
}
