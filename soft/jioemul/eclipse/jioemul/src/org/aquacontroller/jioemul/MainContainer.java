/**
 * 
 */
package org.aquacontroller.jioemul;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JToggleButton;

/**
 * @author george
 *
 */
public class MainContainer extends java.awt.Container {

	private static final long serialVersionUID = 719271647091662577L;
	private static final Object BUTTONS_LEFT[]=new Object[]{
			new Object[] { "KEY_1", 1 }, 
			new Object[] { "KEY_2", 2 },
			new Object[] { "KEY_3", 4 }, 
			new Object[] { "KEY_4", 8 }
	};
	private static final Object BUTTONS_RIGHT[]=new Object[]{
			new Object[] { "KEY_5", 16 }, 
			new Object[] { "KEY_6", 32 },
			new Object[] { "KEY_7", 64 }, 
			new Object[] { "KEY_8", 128 }
	};
	private final LcdWidget mLcdWidget;
	private IButtonHandler mButtonHandler;
	private final Container mLeftButtonsContainer;
	private final Container mRightButtonsContainer;
	private final Triggers mTrigger;
	
	/**
	 * 
	 */
	public MainContainer() {
		setLayout(new BorderLayout());
		mLcdWidget = new LcdWidget();
		mTrigger = new Triggers();
		mLeftButtonsContainer = new Container();
        fillButtonContainer(mLeftButtonsContainer, BUTTONS_LEFT);
		mLeftButtonsContainer.setLayout(new BoxLayout(mLeftButtonsContainer, BoxLayout.Y_AXIS));
		add(mLeftButtonsContainer, BorderLayout.WEST);

		add(mLcdWidget, BorderLayout.CENTER);
		
		add(mTrigger.getContainer(), BorderLayout.NORTH);

		mRightButtonsContainer = new Container();
        fillButtonContainer(mRightButtonsContainer, BUTTONS_RIGHT);
		mRightButtonsContainer.setLayout(new BoxLayout(mRightButtonsContainer, BoxLayout.Y_AXIS));
		add(mRightButtonsContainer, BorderLayout.EAST);
        
        Container southContainer = new Container();
        AbstractButton buttonReset = new JButton("Reset");
        buttonReset.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    mLcdWidget.reset();
                }
            });
        southContainer.add(Box.createHorizontalGlue());
        southContainer.add(buttonReset);
        southContainer.add(Box.createHorizontalGlue());
        southContainer.setLayout(new BoxLayout(southContainer, BoxLayout.X_AXIS));
        add(southContainer, BorderLayout.SOUTH);
	}

	public IDisplay getDisplay() {
		return mLcdWidget;
	}
	
	public ITrigger getTrigger() {
		return mTrigger;
	}
	
	public void setButtonHandler(IButtonHandler buttonHandler) {
		mButtonHandler = buttonHandler;
	}
	
    private void fillButtonContainer(Container container, Object []buttons) {
		for (int i = 0; i < buttons.length; i++) {
			Object btnRec[] = (Object[])buttons[i];
			AbstractButton button = new JToggleButton();
			button.setText((String)btnRec[0]);
			button.setActionCommand(btnRec[1].toString());
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent actionevent) {
					buttonPress(actionevent);
				}
			});
            container.add(Box.createVerticalGlue());
			container.add(button);
		}
        container.add(Box.createVerticalGlue());
    }

	private void buttonPress(ActionEvent event) {
		int nChildren;
		byte bitMask = 0;

		if (mButtonHandler == null) {
			return;
		}

		nChildren = mLeftButtonsContainer.getComponentCount();
		for (int i = 0; i < nChildren; i++) {
			Component component = mLeftButtonsContainer.getComponent(i);
			if (component instanceof JToggleButton) {
				JToggleButton btn = (JToggleButton) component;
				if (btn.isSelected()) {
					bitMask |= (byte) Integer.parseInt(btn.getActionCommand());
				}
			}
		}
		nChildren = mRightButtonsContainer.getComponentCount();
		for (int i = 0; i < nChildren; i++) {
			Component component = mRightButtonsContainer.getComponent(i);
			if (component instanceof JToggleButton) {
				JToggleButton btn = (JToggleButton) component;
				if (btn.isSelected()) {
					bitMask |= (byte) Integer.parseInt(btn.getActionCommand());
				}
			}
		}
		mButtonHandler.buttonPress(bitMask);
	}
}
