/**
 * 
 */
package org.aquacontroller.jioemul;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author george
 * 
 */
public class DisplayServer implements Runnable, IButtonHandler {

    private final ServerSocket mSocket;
    private final IDisplay mDisplay;
    private final ITrigger mTrigger;
	private OutputStream mOutputStream;

    public DisplayServer(int port, IDisplay display, ITrigger trigger) throws IOException {
        mSocket = new ServerSocket(port);
        mDisplay = display;
        mTrigger = trigger;
        new Thread(this).start();
    }

    /*
     * (non-Javadoc)
     * @see org.aquacontroller.jioemul.IButtonHandler#buttonPress(int)
     */
	public void buttonPress(byte flags) {
		if (mOutputStream != null) {
			try {
				mOutputStream.write(("GB" + Utils.byteToString(flags) + "\n")
						.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/*
     * (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    public void run() {
        byte bufCmd[] = new byte[2];
        Socket sock = null;
        InputStream is;
        
        mOutputStream = null;
        while (true) {
            if (sock != null) {
                try {
                	mOutputStream = null;
                    sock.close();
                } catch (IOException e) {
                }
                sock = null;
            }
            try {
                sock = mSocket.accept();
                is = sock.getInputStream();
                mOutputStream = sock.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }

            boolean isClose = false;
            while (!isClose) {
                try {
                    int rb = is.read(bufCmd);
//                    System.out.println("DisplayServer.run() rb=" + rb);
                    if (rb == 1) {
                        switch (bufCmd[0]) { // skip LF
                            case 10:
                                continue;
                            case 4:
                                System.out.println("Connection close request");
                                isClose = true;
                                continue;
                        }
                    }
                    
                    if (rb != bufCmd.length) {
                        System.out.println("Read command failed: rb=" + rb
                                + " expected=" + bufCmd.length);
                        for (int i = 0; i < rb; i++) {
                            System.out.println(bufCmd[i]);
                        }
                        break;
                    }
                    parseCmd(bufCmd, is, mOutputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                } catch (Throwable e) {
                    e.printStackTrace();
                    writeErr(mOutputStream);
                    break;
                }
            }
        }
    }

    private void parseCmd(byte[] cmd, InputStream is, OutputStream os) {
        switch (cmd[0]) {
            case 'G':
                // get command
                parseCmdGet(cmd[1], is, os);
                break;
            case 'S':
                // set command
                parseCmdSet(cmd[1], is, os);
                break;
            default:
                System.out.println("Unknown command: " + (char) cmd[0]);
                writeErr(os, "CMD " + (char) cmd[0]);
        }
    }

    private void parseCmdGet(byte cmd, InputStream is, OutputStream os) {
        switch (cmd) {
            case 'D':
                byte data = mDisplay.getData(Utils.byteToInt(readAsciiByte(is)));
                try {
                    String out = Utils.byteToString(data) + "\n";
                    os.write(out.getBytes());

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Unknown GET command: " + (char) cmd);
                writeErr(os, "GET " + (char) cmd);
        }
        readEof(is);
    }

    private void parseCmdSet(byte cmd, InputStream is, OutputStream os) {
		switch (cmd) {
		case 'D': // set data
			mDisplay.setData(Utils.byteToInt(readAsciiByte(is)),
					readAsciiByte(is));
			writeOk(os);
			break;
		case 'P': // set page
			mDisplay.setPage(Utils.byteToInt(readAsciiByte(is)), Utils
					.byteToInt(readAsciiByte(is)));
			writeOk(os);
			break;
		case 'X': // set X pos
			mDisplay.setX(Utils.byteToInt(readAsciiByte(is)), Utils
					.byteToInt(readAsciiByte(is)));
			writeOk(os);
			break;
		case 'T': // set triggers
			mTrigger.setTriggers(readAsciiByte(is));
			writeOk(os);
			break;
		default:
			System.out.println("Unknown SET command: " + (char) cmd);
			writeErr(os, "SET " + (char) cmd);
		}
		readEof(is);
    }

    private byte readAsciiByte(InputStream is) {
        byte buf[] = new byte[2]; // FA
        byte data;
        try {
            if (is.read(buf) != buf.length) {
                System.err.println("Failed to read Ascii byte!");
                return 0;
            }
            data = Utils.StringToByte(new String(buf));
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
        return data;
    }

    private void readEof(InputStream is) {
    	try {
			is.read(); //read \r
	    	is.read(); //read \n
		} catch (IOException e) {
			e.printStackTrace();
		} 
    }
    
    private void writeOk(OutputStream os) {
        try {
            os.write("OK\n".getBytes());
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeErr(OutputStream os) {
        writeErr(os, null);
    }

    private void writeErr(OutputStream os, String msg) {
        StringBuilder buf = new StringBuilder();
        buf.append("ERROR");
        if (msg != null) {
            buf.append(" ").append(msg);
        }
        buf.append("\n");

        try {
            os.write(buf.toString().getBytes());
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
