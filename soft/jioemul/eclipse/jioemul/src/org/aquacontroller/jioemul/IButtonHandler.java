/**
 * 
 */
package org.aquacontroller.jioemul;

/**
 * @author george
 *
 */
public interface IButtonHandler {
	public void buttonPress(byte flags);
}
