package org.aquacontroller.jioemul;

import java.awt.Container;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JRadioButton;

public class Triggers implements ITrigger {

	private static final int NUM_TRIGGERS = 8;
	
	private final Container mContainer;
	private final JRadioButton[] mButtons;
	
	public Triggers() {
		mContainer = new Container();
		mContainer.setLayout(new BoxLayout(mContainer, BoxLayout.X_AXIS));
		mButtons = new JRadioButton[NUM_TRIGGERS];
		mContainer.add(Box.createHorizontalGlue());
		for (int i = 0; i < NUM_TRIGGERS; i++) {
			JRadioButton btn = new JRadioButton();
			btn.setEnabled(false);
			btn.setSelected(false);
			mButtons[i] = btn;
			mContainer.add(btn);
		}
		mContainer.add(Box.createHorizontalGlue());
	}
	
	public Container getContainer() {
		return mContainer;
	}
	
	@Override
	public void setTriggers(byte mask) {
		System.out.println("setTriggers: " + Utils.byteToString(mask));
		for (int i = 0; i < NUM_TRIGGERS; i++) {
			// System.out.println("i=" + i + " " + (1 >> i) + " " + ((1 >> i) & mask));
			if (((1 << i) & mask) != 0) {
				mButtons[NUM_TRIGGERS - i - 1].setSelected(true);
			} else {
				mButtons[NUM_TRIGGERS - i - 1].setSelected(false);
			}
		}
	}

}
