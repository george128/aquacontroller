/**
 * 
 */
package org.aquacontroller.jioemul;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

/**
 * @author george
 *
 */
public class LcdWidget extends JComponent implements IDisplay {

	private static final long serialVersionUID = 587340519305660127L;
	private static final int WIDTH = 64;
	private static final int HEIGHT = 64;
	private static final int NUM_PAGES = HEIGHT / 8;
	private static final int NUM_CONTROLLERS = 2;
	private static final int SCALE = 4;
	
	private byte mBuffer[][] = new byte[NUM_CONTROLLERS][WIDTH * NUM_PAGES];
	private int mPage[] = new int[NUM_CONTROLLERS];
	private int mX[] = new int[NUM_CONTROLLERS];
	
	public LcdWidget() {
		Dimension size = new Dimension(WIDTH * SCALE * NUM_CONTROLLERS, 
				HEIGHT * SCALE);
		setMaximumSize(size);
		setMinimumSize(size);
		setPreferredSize(size);
        reset();
	}

    public void reset() {
		for (int i = 0; i < NUM_CONTROLLERS; i++) {
			mPage[i] = 0;
			mX[i] = 0;
		}

		Random rnd = new Random();
		for (int i = 0; i<mBuffer.length;i++ ) {
			rnd.nextBytes(mBuffer[i]);
		}

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				repaint();
			}
		});
    }
	
	public void setPage(int controller, int page) {
//	    System.out.println("LcdWidget.setPage(): [" + controller + "] " + page);
		if (controller >= NUM_CONTROLLERS) {
			throw new RuntimeException("Wrong controller id: " + controller
					+ " of " + NUM_CONTROLLERS);
		}
	    if (page >= NUM_PAGES) {
            throw new RuntimeException("Wrong page: " + page + " of "
                    + NUM_PAGES);
        }
		mPage[controller] = page;
	}
	
	public void setX(int controller, int x) {
//		System.out.println("LcdWidget.setX(): [" + controller + "] " + x);
		if (controller >= NUM_CONTROLLERS) {
			throw new RuntimeException("Wrong controller id: " + controller
					+ " of " + NUM_CONTROLLERS);
		}
		if (x >= WIDTH) {
			throw new RuntimeException("Wrong X position: " + x + " of "
					+ WIDTH);
		}
		mX[controller] = x;
	}

	public void setData(int controller, byte data) {
		if (controller >= NUM_CONTROLLERS) {
			throw new RuntimeException("Wrong controller id: " + controller
					+ " of " + NUM_CONTROLLERS);
		}
		mBuffer[controller][mPage[controller] * WIDTH + mX[controller]] = data;
		increaseAddr(controller);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				repaint();
			}
		});
	}

	public byte getData(int controller) {
		if (controller >= NUM_CONTROLLERS) {
			throw new RuntimeException("Wrong controller id: " + controller
					+ " of " + NUM_CONTROLLERS);
		}
		byte data = mBuffer[controller][mPage[controller] * WIDTH
				+ mX[controller]];
		increaseAddr(controller);
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	protected void paintComponent(Graphics g) {
//		System.out.println("LcdWidget.paintComponent()");
		g.setColor(Color.LIGHT_GRAY);
		for (int i = 0; i < WIDTH * SCALE * NUM_CONTROLLERS; i += SCALE) {
			g.drawLine(i, 0, i, HEIGHT * SCALE);
		}
		for (int i = 0; i < HEIGHT * SCALE; i += SCALE) {
			g.drawLine(0, i, WIDTH * NUM_CONTROLLERS * SCALE, i);
		}

		g.setColor(Color.BLACK);
		for (int controller = 0; controller < NUM_CONTROLLERS; controller++) {
			byte controllerBuf[] = mBuffer[controller];
			for (int page = 0; page < NUM_PAGES; page++) {
				for (int x = 0; x < WIDTH; x++) {
					byte data = controllerBuf[page * WIDTH + x];
					for (int bit = 0; bit < 8; bit++) {
						if ((data & (1 << bit)) != 0) {
							g.fillRect((x + controller * WIDTH) * SCALE,
									(page * 8 + bit) * SCALE, SCALE, SCALE);
						} else {
							g.clearRect(((x + controller * WIDTH) * SCALE) + 1,
									(page * 8 + bit) * SCALE + 1, SCALE - 1,
									SCALE - 1);
						}
					}
				}
			}
		}
	}
	
	private void increaseAddr(int controller) {
		mX[controller]++;
//		if (mX[controller] == WIDTH) {
//			mX[controller] = 0;
//			mPage[controller]++;
//			if (mPage[controller] == NUM_PAGES) {
//				mPage[controller] = 0;
//			}
//		}
//		System.out.println("LcdWidget.increaseAddr() [" + controller
//				+ "] page=" + mPage[controller] + " X=" + mX[controller]);
	}
}
