package org.aquacontroller.jioemul;

public class Utils {

    private static final String HEX="0123456789ABCDEF";
    
    public static String byteToString(byte b) {
        StringBuilder buf = new StringBuilder();
        
        buf.append(HEX.charAt((b >> 4) & 0x0f)).append(HEX.charAt(b & 0x0f));
        
        return buf.toString();
        
    }

    public static byte StringToByte(String str) {
        int b1, b2;

        str = str.toUpperCase();
        b1 = HEX.indexOf(str.charAt(0));
        b2 = HEX.indexOf(str.charAt(1));
//        System.out.println("Utils.StringToByte() str='" + str + "' b1=" + b1
//                + " b2=" + b2);
        if (b1 < 0 || b2 < 0) {
            System.err.println("Wrong byte string: " + str);
            return 0;
        }

        return (byte) (b1 * 16 + b2);
    }
    
    public static int byteToInt(byte b) {
        int ret = (int) b;
        if (ret < 0)
            ret += 256;
        return ret;
    }
}
