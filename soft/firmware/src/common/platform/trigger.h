#ifndef _TRIGGER_H_
#define _TRIGGER_H_

void trigger_init();
void trigger_set(char num, unsigned char state);
void trigger_set_all(unsigned char all_state);
#endif /*_TRIGGER_H_*/
