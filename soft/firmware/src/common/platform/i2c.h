#ifndef _I2C_H_
#define _I2C_H_

/* i2c clock speed */
#define I2C_RATE 100000UL

#define I2C_DEV_NO_DS1307 0xd0
#define I2C_DEV_NO_24C32 0xa0
#define I2C_DEV_NO_BMP085 0xee


void i2c_init();

unsigned char i2c_read(unsigned char dev_no, 
                       unsigned char addr, 
                       unsigned char cnt, 
                       char *buf);
unsigned char i2c_write(unsigned char dev_no, 
                       unsigned char addr, 
                       unsigned char cnt, 
                       char *buf);


#endif /*_I2C_H_*/
