#ifndef _DISPLAY_H_
#define _DISPLAY_H_

void display_init();
void display_set_key_handler(void (*key_handler)(char flags));
void display_set_page(char controller, char page);
void display_set_addr(char controller, char addr);
void display_set_data(char controller, char data);
char display_get_data(char controller);

#endif /*_DISPLAY_H_*/
