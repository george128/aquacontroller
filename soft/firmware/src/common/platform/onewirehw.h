#ifndef _ONEWIREHW_H_
#define _ONEWIREHW_H_

void onewirehw_write_bit(char bit);
char onewirehw_read_bit();
char onewirehw_reset();

#endif /*_ONEWIRENW_H_*/
