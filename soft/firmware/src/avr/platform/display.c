#include <avr/io.h>
#include <util/delay.h>

#include <utils/strings.h>
#include <console/console.h>
#include <platform/display.h>
#include "display.h"
#include "keyboard.h"

static inline void chip_select(char controller)
{
    if(controller)
        LCD_CMD|=_BV(LCD_CS2);
    else
        LCD_CMD|=_BV(LCD_CS1);
}

static inline void chip_deselect(char controller)
{
    if(controller)
        LCD_CMD&=~_BV(LCD_CS2);
    else
        LCD_CMD&=~_BV(LCD_CS1);
}

static inline void set_read()
{
    LCD_CMD|=_BV(LCD_RW);
}

static inline void set_write()
{
    LCD_CMD&=~_BV(LCD_RW);
}

static inline void set_cmd()
{
    LCD_CMD&=~_BV(LCD_DI);
}

static inline void set_data()
{
    LCD_CMD|=_BV(LCD_DI);
}

static inline void set_e()
{
    LCD_CMD|=_BV(LCD_E);
}

static inline void clear_e()
{
    LCD_CMD&=~_BV(LCD_E);
}

static void strobe()
{
    set_e();
    _delay_us(5);
    clear_e();
    _delay_us(5);
}

static void wait_busy(char controller)
{
    LCD_DATA_DIR=0;
    set_cmd();
    set_read();
    chip_select(controller);
    char status;

    do
    {
        _delay_us(50);
        set_e();
        _delay_us(50);
        status = LCD_DATA_IN;
        clear_e();
    } while(status & LCD_BUSY);
    chip_deselect(controller);
    LCD_DATA_DIR=0xff;
    _delay_us(50);
}

static void write_cmd(char controller, unsigned char cmd) 
{
    set_cmd();
    set_write();
    chip_select(controller);
    LCD_DATA=cmd;
    _delay_us(1);
    strobe();
    chip_deselect(controller);
}

static void write_data(char controller, unsigned char data) 
{
    set_data();
    set_write();
    chip_select(controller);
    _delay_us(1);
    LCD_DATA=data;
    strobe();
    chip_deselect(controller);
}

static unsigned char read_data(char controller)
{
    unsigned char data;
    
    // wait_busy(controller);
    set_data();
    set_read();
    set_e();
    LCD_DATA_DIR=0;
    LCD_DATA=0xff;
    chip_select(controller);
    _delay_us(100);
    clear_e();
    _delay_us(100);
    set_e();
    _delay_us(100);

    data=LCD_DATA_IN;
    _delay_us(20);
    clear_e();
    //_delay_us(100);
    chip_deselect(controller);
    LCD_DATA_DIR=0xff;
    
    return data;
}

static void write_xy(char controller, unsigned char x, unsigned char y) 
{
    write_cmd(controller, 0xb8+y);
    write_cmd(controller, 0x40+x);
}

void display_init()
{
    console_puts_P(STR_INIT_DISPLAY);

    LCD_CMD_DIR|=_BV(LCD_RST);
	LCD_CMD_DIR|=_BV(LCD_E);
    LCD_CMD_DIR|=_BV(LCD_RW);
	LCD_CMD_DIR|=_BV(LCD_DI);
	LCD_CMD_DIR|=_BV(LCD_CS1);
	LCD_CMD_DIR|=_BV(LCD_CS2);
    LCD_DATA_DIR=0xff;

    chip_deselect(0);
    chip_deselect(1);

    LCD_CMD|=_BV(LCD_RST);
    _delay_ms(5);

    write_cmd(0, 0x3f);
    write_xy(0, 0, 0);
    write_cmd(0, 0xc0);

    write_cmd(1, 0x3f);
    write_xy(1, 0, 0);
    write_cmd(1, 0xc0);

    avr_keyboard_init();
}

void display_set_key_handler(void (*key_handler)(char flags))
{
    avr_set_key_handler(key_handler);
}

void display_set_page(char controller, char page)
{
    write_cmd(controller,0xb8+page);
}

void display_set_addr(char controller, char addr)
{
    write_cmd(controller,0x40+addr);
}

void display_set_data(char controller, char data)
{
    write_data(controller, data);
}

char display_get_data(char controller)
{
    return read_data(controller);
}
