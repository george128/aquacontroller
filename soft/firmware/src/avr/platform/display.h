#ifndef _AVR_DISPLAY_H_
#define _AVR_DISPLAY_H_

#include <avr/io.h>

#define LCD_RST 7
#define LCD_E   4
#define LCD_RW  3
#define LCD_DI  2
#define LCD_CS2 6
#define LCD_CS1 5

#define LCD_DATA PORTC
#define LCD_DATA_IN PINC
#define LCD_DATA_DIR DDRC

#define LCD_CMD PORTA
#define LCD_CMD_DIR DDRA

#define LCD_BUSY _BV(7)

#endif /*_AVR_DISPLAY_H_*/
