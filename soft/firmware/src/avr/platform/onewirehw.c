#include <avr/io.h>
#include <avr/cpufunc.h>
#include <util/delay.h>

#include <onewire/onewire.h>
#include <platform/onewirehw.h>

#define PIN 4
#define PORT_OUT PORTB
#define PORT_DIR DDRB
#define PORT_IN PINB

static inline void pin_high() {
    PORT_OUT |=_BV(PIN);
}
static inline void pin_low() {
    PORT_OUT &=~_BV(PIN);
}
static inline void set_out() {
    PORT_DIR |=_BV(PIN);
    _NOP();
}
static inline void set_in() {
    PORT_DIR &=~_BV(PIN);
    _NOP();
}
static inline char pin_read() {
    return PORT_IN & _BV(PIN) ? 1 : 0;
}


char onewirehw_reset()
{
    char presence;
    pin_high();
    set_out();
    pin_low();
    _delay_us(500);
    pin_high();
    set_in();
    _delay_us(65);
    presence=pin_read() ? 0 : 1;
    _delay_us(200);
    return presence;
}

void onewirehw_write_bit(char bit)
{
    set_out();
    pin_low();
    if(bit) {
        _delay_us(15);
        set_in();
        _delay_us(60);
    } else {
        _delay_us(75);
        set_in();
    }
    pin_high();
    _delay_us(2);
}

char onewirehw_read_bit()
{
    char bit;
    set_out();
    pin_low();
    _delay_us(2);
    set_in();
    pin_high();
    _delay_us(20);
    bit=pin_read();
    _delay_us(30);
    while(!pin_read());
    _delay_us(2);
    return bit;
}
