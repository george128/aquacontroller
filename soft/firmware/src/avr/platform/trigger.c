#include <avr/io.h>
#include <platform/trigger.h>
#include <console/console.h>
#include <utils/strings.h>

#define set_port(p, state) \
    { if(state)                 \
          PORTF|=_BV(p);        \
    else                        \
        PORTF&=~_BV(p); }

void trigger_init()
{
    DDRF=0xff;
    console_puts_P(STR_TRIGGER_INIT);
    trigger_set_all(0);
}

void trigger_set(char num, unsigned char state)
{
    state=state?0:1;
    switch(num) {
        case 0:
            set_port(0, state);
            break; 
        case 1:
            set_port(1, state);
            break;
        case 2:
            set_port(2, state);
            break;
        case 3:
            set_port(3, state);
            break;
        case 4:
            set_port(4, state);
            break;
        case 5:
            set_port(5, state);
            break;
        case 6:
            set_port(6, state);
            break;
        case 7:
            set_port(7, state);
            break;
        default:
            ;
    };
}

void trigger_set_all(unsigned char all_state)
{
    unsigned char st=all_state^0xff;
#if defined DEBUG_TRIGGER
    console_putd(st);
    console_putc('\n');
#endif
    PORTF=st;
}

