#include <console/console.h>

#include <avr/io.h>
#include <avr/pgmspace.h>

static void uart0_init()
{
#define BAUD_TOL 3 /*to avoid compiler warning*/
#define BAUD 115200
#include <util/setbaud.h>

    UBRR0 = UBRR_VALUE;
#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~_BV(U2X0);
#endif
    UCSR0B = _BV(RXEN0)|_BV(TXEN0);
}

static void uart0_putc(char ch)
{
    while(!(UCSR0A & _BV(UDRE0))); /*wait unitil transmitter be empty*/
    UDR0 = ch;
}

static char uart0_input_check(char timeout_ms)
{
    return (UCSR0A & _BV(RXC0));
}

static char uart0_readc()
{
    while (!uart0_input_check(0)); /* Wait for data to be received */
    return UDR0;
}

PROGMEM static const struct console_dev console_uart0 =
{
    .init=uart0_init,
    .putc=uart0_putc,
    .readc=uart0_readc,
    .input_check=uart0_input_check,
};

/*------------------------------------------------------------
bluetooth module
--------------------------------------------------------------*/

static char bt_connected;

static void uart3_init()
{
#undef BAUD_TOL
#undef BAUD
#define BAUD 9600
#include <util/setbaud.h>
    UBRR3 = UBRR_VALUE;
#if USE_2X
    UCSR3A |= _BV(U2X3);
#else
    UCSR3A &= ~_BV(U2X3);
#endif
    UCSR3B = _BV(RXEN3)|_BV(TXEN3);
    bt_connected = 1;
}

static void uart3_putc(char ch)
{
    if(!bt_connected)
        return;
    while(!(UCSR3A & _BV(UDRE3))); /*wait unitil transmitter be empty*/
    UDR3 = ch;
}

static char uart3_input_check(char timeout_ms)
{
    return (UCSR3A & _BV(RXC3));
}

static char uart3_readc()
{
    while (!uart3_input_check(0)); /* Wait for data to be received */
    return UDR3;
}

PROGMEM static const struct console_dev console_uart3 =
{
    .init=uart3_init,
    .putc=uart3_putc,
    .readc=uart3_readc,
    .input_check=uart3_input_check,
};
/*
------------------------------------------------------
*/
void console_platform_init()
{
    register_console(&console_uart0);
    register_console(&console_uart3);
}
