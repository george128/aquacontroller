#ifndef _UTIL_H_
#define _UTIL_H_

#include <avr/pgmspace.h>

#define read_rom(a) pgm_read_byte(a)

#define debug_print(a, ...) 
#define debug_puts(a)

#ifndef NULL
#define NULL ((void*)0)
#endif /*NULL*/

#endif /*_UTIL_H_*/
