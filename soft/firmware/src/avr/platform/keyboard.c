#include <avr/io.h>
#include <stdlib.h>

#include <utils/strings.h>
#include <console/console.h>
#include <timer/timer.h>

#include "keyboard.h"

static void (*current_key_handler)(char flags) = NULL;

static struct timer timer_keyb;

static void avr_keyboard_timer_callback(unsigned char timer_id, void *data)
{
    if(current_key_handler != NULL)
        current_key_handler(PINL);
}


void avr_keyboard_init()
{
    console_puts_P(STR_INIT_KEYBOARD);
    DDRL = 0;
    PORTL = 0xff;
    timer_keyb.period=200;
    timer_keyb.callback=avr_keyboard_timer_callback;
    timer_setup(&timer_keyb);
}

void avr_set_key_handler(void (*key_handler)(char flags)) {
    current_key_handler = key_handler;
}
