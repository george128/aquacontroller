#include <platform/utils.h>
#include <console/console.h>
#include <utils/strconv.h>
#include <utils/strings.h>

#define MAX_CONSOLES 2

static struct console_dev consoles[MAX_CONSOLES];
static unsigned char console_idx;

static void console_puts_no_zero(char *buf) {
    char *ptr = buf;
    while(*ptr=='0')ptr++;
    console_puts(ptr);
}

void console_init()
{
    console_idx = 0;
    debug_puts("console_init: started\n");
    console_platform_init();
    console_puts_P(STR_CONSOLE_INIT1);
    console_putd(console_idx);
    console_puts_P(STR_CONSOLE_INIT2);
}

void register_console(const struct console_dev *dev)
{
    if(console_idx == MAX_CONSOLES) {
        debug_puts("register console: stack overflow!!!\n");
        return;
    }
    memcpy_P(consoles+console_idx,dev,sizeof(*dev));
    consoles[console_idx].init();
    console_idx++;
}



void console_putc(char ch)
{
    unsigned char i;
    void (*func)(char c);

    for(i=0;i<console_idx;i++) {
        func=consoles[i].putc;
        if(func != NULL) {
            func(ch);
        }
    }
}

char console_read(char timeout_ms)
{
    unsigned char i;
    char (*func_read)();
    char (*func_check)(char);

    for(i=0;i<console_idx;i++) {
        func_check=consoles[i].input_check;
        if(func_check != NULL && func_check(timeout_ms)) {
            func_read=consoles[i].readc;
            if(func_read != NULL) {
                return func_read();
            }
        }
    }
    return -1;
}


void console_puts(const char *str)
{
    char ch;
    while((ch=*(str++))!=0) {
        console_putc(ch);
    }
}

void console_puts_P(const char *str)
{
    char ch;
    while((ch=pgm_read_byte(str++))!=0) {
        console_putc(ch);
    }
}

void console_putd(char byte)
{
    char str[3];
    console_puts(byte2str(byte, str));
}

void console_putd10(char d)
{
    char buf[4];
    
    buf[3]=0;
    for(char i=2; i>=0; i--) {
        buf[i]=byte2char(d%10);
        d/=10;
    }
    console_puts_no_zero(buf);
}

void console_putmem(const void *ptr, char size)
{
    char i;
    for(i=0;i<size;i++) {
        if(i>0)
            console_putc(' ');
        console_putd(((char *)ptr)[i]);
    }
}

void console_put_short(unsigned short d)
{
    char buf[6];

    buf[5]=0;
    for(char i=4; i>=0; i--) {
        buf[i]=byte2char(d%10);
        d/=10;
    }
    console_puts_no_zero(buf);
}

void console_put_long(unsigned long d)
{
    char buf[20];

    buf[19]=0;
    for(char i=18; i>=0; i--) {
        buf[i]=byte2char(d%10);
        d/=10;
    }
    console_puts_no_zero(buf);
}
