#include <stdlib.h>

#include <platform/display.h>
#include <platform/utils.h>
#include <string.h>
#include <assert.h>

#include "graphics.h"

/*#define abs(x) (x>0?x:-x)*/

#define SWAP(x,y) { char tmp=x;x=y;y=tmp;}

#define RAM_SIZE SCREEN_HEIGHT*SCREEN_WIDTH/LINE_HEIGHT
static char RAM_BUF[RAM_SIZE];

static char flush_auto=0;

void graphics_init()
{
    display_init();
    clear_screen();
    flush_screen();
}

void graphics_flush_auto(char is_flush)
{
    flush_auto = is_flush;
}

#ifdef GRAPHICS_DEBUG
static void test1()
{
    unsigned char t[8] = {255, 127, 63, 31, 15, 7, 3 ,1};

    set_pos(0, 0);
    for(int i=0; i<64; i++)
    {
        display_set_data(0, t[i%8]);
    }
    while(1) {
        char data[64];
        set_pos(0, 0);
        for(int i=0;i<64;i++)
        {
            data[i]=display_get_data(0);
        }
        set_pos(0, 8);
        for(int i=0;i<64;i++)
        {
            display_set_data(0, data[i]);
        }
        flush_screen();
    }
}

static void test2()
{
    // draw_text(7,8, "ABCDEFGHIJabcdefghij",1);
    // draw_text(0,16, "ABCDEFGHIJabcdefghij01234567890123456789",1);
    draw_line(0,0,127,63);
    
    // draw_text(8,35,"abcdefghijklmnopqrstuvwxyz",1);
    flush_screen();
}

void graphics_test()
{
    test2();
}
#endif

static inline void check_pos(int pos) {
    assert(pos<RAM_SIZE);
}

static char get_data(char x, char y)
{
    int pos=(y/8)*SCREEN_WIDTH+x;
    check_pos(pos);
    return RAM_BUF[pos];
}

static void set_data(char x, char y, char data)
{
    int pos=(y/8)*SCREEN_WIDTH+x;
    check_pos(pos);
    RAM_BUF[pos]=data;
}

static void check_flush() {
    if(flush_auto) {
        flush_screen();
    }
}

void set_pos(char x, char y)
{
    char page=y/8;
    char controller=x/CONTOLLER_WIDTH;

    x=x%64;
    display_set_page(controller, page);
    display_set_addr(controller, x);
}


void draw_pixel(char x, char y, char is_set)
{
    char data;
    char bit=1<<(y%8);

    data=get_data(x, y);

    if(is_set)
        data|=bit;
    else
        data&=0xff^bit;
    
    set_data(x, y, data);
    check_flush();
}

void draw_line(char x0, char y0, char x1, char y1)
{
    char Dx = x1 - x0; 
    char Dy = y1 - y0;
    char steep = (abs(Dy) >= abs(Dx));
    char xstep = 1;
    char ystep = 1;
    char TwoDy; 
    char TwoDyTwoDx;
    char E;
    char x,y;
    char xDraw, yDraw;	
    char flush_save = flush_auto;
    flush_auto = 0;

    if (steep) {
        SWAP(x0, y0);
        SWAP(x1, y1);
        /* recompute Dx, Dy after swap */
        Dx = x1 - x0;
        Dy = y1 - y0;
    }
    if (Dx < 0) {
        xstep = -1;
        Dx = -Dx;
    }
    if (Dy < 0) {
        ystep = -1;		
        Dy = -Dy; 
    }
    TwoDy = 2*Dy; 
    TwoDyTwoDx = TwoDy - 2*Dx; /* 2*Dy - 2*Dx */
    E = TwoDy - Dx; /* 2*Dy - Dx */
    y = y0;
    for (x = x0; x != x1; x += xstep) {		
        if (steep) {			
            xDraw = y;
            yDraw = x;
        } else {			
            xDraw = x;
            yDraw = y;
        }
        /* plot */
        /* plot(xDraw, yDraw); */
        draw_pixel(xDraw, yDraw, 1);
        /* next */
        if (E > 0) {
            E += TwoDyTwoDx; /* E += 2*Dy - 2*Dx; */
            y = y + ystep;
        } else {
            E += TwoDy; /* E += 2*Dy; */
        }
    }
    flush_auto = flush_save;
    check_flush();
}

void draw_char(char x, char y, char c)
{
    unsigned char i;
    unsigned char offset=y&7;
    //debug_print("draw_char x=%d y=%d '%c'\n", x, y, c);
    c -= 32;
    if(offset) {
        unsigned char mask_up=(1<<(offset))-1;
        unsigned char mask_low=255-mask_up;
        unsigned char y_up=y&0xf8;
        unsigned char y_low=y_up+8;

        for(i=0; i<FONT_WIDTH +1; i++,x++) {
            unsigned char font_data=i<FONT_WIDTH?read_rom((char *)((int)font5x8 + (FONT_WIDTH * c) + i)):0;
            unsigned char data;

            /*upper*/
            data = get_data(x, y_up);
            data &= mask_up;
            data |= font_data<<offset;
            set_data(x, y_up, data);

            /*lower*/
            data = get_data(x, y_low);
            data &= mask_low;
            data |= font_data>>(8-offset);
            set_data(x, y_low, data);
        }
    } else {
        set_pos(x,y);
        for(i=0; i<FONT_WIDTH; i++,x++) {
            set_data(x, y, read_rom((char *)((int)font5x8 + (FONT_WIDTH * c) + i)));
        }

        set_data(x, y ,0);
    }
    check_flush();
}

static void draw_text_aligned(char x, char y, const char *text, 
                              char is_wrap, char is_progmem)
{
    char i;
    char c;
    
    for(c=is_progmem?read_rom(text):*text;
        c;
        c=is_progmem?read_rom(++text):*(++text)) {
        
        if(c=='\n' || x+CHAR_WIDTH>=SCREEN_WIDTH) {
            if(is_wrap) {
                x=0;
                y+=LINE_HEIGHT;
                if(y>SCREEN_HEIGHT)
                    break;
            } else
                break;
        }
        
        if(c=='\r' || c=='\n')
            continue;

        /* debug_print("draw_text_aligned: x=%d y=%d c='%c'\n",x,y,c); */
        c-=32;
        for(i = 0; i < FONT_WIDTH; i++,x++) {
            set_data(x, y,
                     read_rom((char *)((int)font5x8 + (FONT_WIDTH * c) + i)));
        }

        set_data(x++,y,0);
    }
}

static void draw_text_unaligned(char x, char y, const char *text, 
                                char is_wrap, char is_progmem)
{
    unsigned char xpos=x;
    char c;
    for(c=is_progmem?read_rom(text):*text;
        c;
        c=is_progmem?read_rom(++text):*(++text),xpos+=CHAR_WIDTH) {
        
        if(c=='\n' || xpos+CHAR_WIDTH>=SCREEN_WIDTH) {
            if(is_wrap) {
                xpos=x;
                y+=LINE_HEIGHT;
                if(y>SCREEN_HEIGHT-1)
                    break;
            } else
                break;
        }

        if(c=='\r' || c=='\n') {
            xpos-=CHAR_WIDTH;
            continue;
        }

        draw_char(xpos,y,c);
    }
}

void draw_text(char x, char y, const char *text, char is_wrap, char is_progmem) {
    if(y&7) 
        draw_text_unaligned(x,y,text,is_wrap, is_progmem);
    else
        draw_text_aligned(x,y,text,is_wrap, is_progmem);

    check_flush();
}

void invert_region(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2)
{
    unsigned char x;
    unsigned char y;
    //debug_print("invert_region %d:%d-%d:%d\n",x1,y1,x2,y2);
        
    for(y=y1;y<y2; y+=8) {
        unsigned char offset = y==y1 ? y1&7 : 0;
        unsigned char mask;
        unsigned char mask_up = 0;

        mask=0xff<<offset;
        if(y2&7) {
            if(y==y1 && y1+8>y2)
                mask&=0xff>>(8-(y2&7));
            if(y+8>y2 && y2-y1>8) {
                mask_up=0xff>>(8-(y2&7));
            }
        }

        //debug_print("invert_region: y=%d offset=%d mask=%02x mask_up=%02x\n", y, offset, mask, mask_up);
        for(x=x1; x<x2; x++) {
            unsigned char data;

            data = get_data(x, y);
            data ^= mask;
            set_data(x, y, data);
            if(mask_up) {
                data = get_data(x, y+8);
                data ^= mask_up;
                set_data(x, y+8, data);
            }
        }
    }
    check_flush();
}

void draw_pic(char x, char y, char width, char height, char *data)
{

}

void flush_screen()
{
    for(int page=0;page<SCREEN_HEIGHT/8;page++) {
        display_set_page(0,page);
        display_set_page(1,page);
        display_set_addr(0,0);
        display_set_addr(1,0);
        for(int x=0;x<CONTOLLER_WIDTH;x++) {
            display_set_data(0,get_data(x, page*8));
            display_set_data(1,get_data(x+CONTOLLER_WIDTH, page*8));
        }
    }
}

void clear_screen()
{
    memset(RAM_BUF,0, RAM_SIZE);
    check_flush();
}

