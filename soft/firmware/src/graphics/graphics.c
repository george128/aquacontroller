#include <platform/display.h>
#include <platform/utils.h>

#include "graphics.h"

#define abs(x) (x>0?x:-x)

#define SWAP(x,y) { char tmp=x;x=y;y=tmp;}

void graphics_init()
{
    display_init();
    clear_screen();
}


static void test1()
{
    char t[8] = {255, 127, 63, 31, 15, 7, 3 ,1};

    set_pos(0, 0);
    for(int i=0; i<64; i++)
    {
        display_set_data(0, t[i%8]);
    }
    while(1) {
        char data[64];
        set_pos(0, 0);
        for(int i=0;i<64;i++)
        {
            data[i]=display_get_data(0);
        }
        set_pos(0, 8);
        for(int i=0;i<64;i++)
        {
            display_set_data(0, data[i]);
        }
    }
}

static void test2()
{
    draw_text(7,8, "ABCDEFGHIJabcdefghij",1);
    draw_text(0,16, "ABCDEFGHIJabcdefghij01234567890123456789",1);
    draw_line(0,0,127,63);
    
    draw_text(8,35,"abcdefghijklmnopqrstuvwxyz",1);
}

void graphics_test()
{
    test2();
}

void set_pos(char x, char y)
{
    char page=y/8;
    char controller=x/CONTOLLER_WIDTH;

    x=x%64;
    display_set_page(controller, page);
    display_set_addr(controller, x);
}


void draw_pixel(char x, char y, char is_set)
{
    char data;
    char bit=1<<(y%8);
    char controller=x/CONTOLLER_WIDTH;

    set_pos(x,y);
    data=display_get_data(controller);

    if(is_set)
        data|=bit;
    else
        data&=0xff^bit;
    
    set_pos(x,y);
    display_set_data(controller, data);
}

void draw_line(char x0, char y0, char x1, char y1)
{
    char Dx = x1 - x0; 
    char Dy = y1 - y0;
    char steep = (abs(Dy) >= abs(Dx));
    char xstep = 1;
    char ystep = 1;
    char TwoDy; 
    char TwoDyTwoDx;
    char E;
    char x,y;
    char xDraw, yDraw;	

    if (steep) {
        SWAP(x0, y0);
        SWAP(x1, y1);
        /* recompute Dx, Dy after swap */
        Dx = x1 - x0;
        Dy = y1 - y0;
    }
    if (Dx < 0) {
        xstep = -1;
        Dx = -Dx;
    }
    if (Dy < 0) {
        ystep = -1;		
        Dy = -Dy; 
    }
    TwoDy = 2*Dy; 
    TwoDyTwoDx = TwoDy - 2*Dx; /* 2*Dy - 2*Dx */
    E = TwoDy - Dx; /* 2*Dy - Dx */
    y = y0;
    for (x = x0; x != x1; x += xstep) {		
        if (steep) {			
            xDraw = y;
            yDraw = x;
        } else {			
            xDraw = x;
            yDraw = y;
        }
        /* plot */
        /* plot(xDraw, yDraw); */
        draw_pixel(xDraw, yDraw, 1);
        /* next */
        if (E > 0) {
            E += TwoDyTwoDx; /* E += 2*Dy - 2*Dx; */
            y = y + ystep;
        } else {
            E += TwoDy; /* E += 2*Dy; */
        }
    }
}
static void draw_text_aligned(char x, char y, const char *text, char is_wrap)
{
    char i;
    char controller=x/CONTOLLER_WIDTH;
    
    set_pos(x,y);
    
    for(;*text;text++,x++) {
	char c=(*text)-32;
	if(x+6>127) {
	    if(is_wrap) {
		x=0;
		y+=8;
		if(y>63)
		    break;
	    } else
		break;
	}

	for(i = 0; i < FONT_WIDTH; i++,x++) {
	    if(controller!=x/CONTOLLER_WIDTH) {
		set_pos(x,y);
		controller=x/CONTOLLER_WIDTH;
	    }
	    display_set_data(controller,
		read_rom((char *)((int)font5x8 + (FONT_WIDTH * c) + i)));
	}

	display_set_data(x/CONTOLLER_WIDTH,0);
    }
}

void draw_char(char x, char y, char c)
{
    unsigned char i;
    unsigned char offset=y&7;

    c -= 32;
    if(offset) {
        unsigned char mask_up=(1<<(offset))-1;
        unsigned char mask_low=255-mask_up;
        unsigned char buf[12], *ptr;
        unsigned char y_up=y&0xf8;
        unsigned char y_low=y_up+8;
        char controller=-1;
        unsigned char xpos;
	
        ptr=buf;
        controller=-1;
        for(i=0,xpos=x; i<FONT_WIDTH+1; i++,xpos++) {
            if(controller!=xpos/CONTOLLER_WIDTH) {
                set_pos(xpos,y_up);
                controller=xpos/CONTOLLER_WIDTH;
            }
            *(ptr++)=display_get_data(controller);
        }
        controller=-1;
        for(i=0,xpos=x; i<FONT_WIDTH+1; i++,xpos++) {
            if(controller!=xpos/CONTOLLER_WIDTH) {
                set_pos(xpos,y_low);
                controller=xpos/CONTOLLER_WIDTH;
            }
            *(ptr++)=display_get_data(controller);
        }

        for(i=0; i<FONT_WIDTH; i++) {
            unsigned char data=read_rom((char *)((int)font5x8 + (FONT_WIDTH * c) + i));
            unsigned char data_up=data<<offset;
            unsigned char data_low=data>>(8-offset);
	    
            buf[i]&=mask_up;
            buf[i+FONT_WIDTH+1]&=mask_low;;
            buf[i]|=data_up;
            buf[i+FONT_WIDTH+1]|=data_low;
        }
        buf[i]&=mask_up;
        buf[i+FONT_WIDTH+1]&=mask_low;
	
        ptr=buf;
	
        controller=-1;
        for(i=0,xpos=x; i<FONT_WIDTH+1; i++,xpos++) {
            if(controller!=xpos/CONTOLLER_WIDTH) {
                set_pos(xpos,y_up);
                controller=xpos/CONTOLLER_WIDTH;
            }
            display_set_data(controller,*(ptr++));
        }
        controller=-1;
        for(i=0,xpos=x; i<FONT_WIDTH+1; i++,xpos++) {
            if(controller!=xpos/CONTOLLER_WIDTH) {
                set_pos(xpos,y_low);
                controller=xpos/CONTOLLER_WIDTH;
            }
            display_set_data(controller,*(ptr++));
        }
    } else {
        set_pos(x,y);
        for(i=0; i<FONT_WIDTH; i++,x++)
            display_set_data(x/CONTOLLER_WIDTH,
                             read_rom((char *)((int)font5x8 + (FONT_WIDTH * c) + i)));

        display_set_data(x/CONTOLLER_WIDTH,0);
    }
}

static void draw_text_unaligned(char x, char y, const char *text, char is_wrap)
{
    unsigned char xpos=x;
    for(;*text; text++,xpos+=FONT_WIDTH+1) {
        if(xpos>128-FONT_WIDTH-1) {
            if(is_wrap) {
                xpos=x;
                y+=8;
                if(y>SCREEN_HEIGHT-1)
                    break;
            } else
                break;
        }
        draw_char(xpos,y,*text);
    }
}

void draw_text(char x, char y, const char *text, char is_wrap) {
    if(y&7) 
        draw_text_unaligned(x,y,text,is_wrap);
    else
        draw_text_aligned(x,y,text,is_wrap);
}


void draw_pic(char x, char y, char width, char height, char *data)
{

}
void clear_screen()
{
    /*
    char controller;
    int px;
    for(controller=0;controller<NUM_CONTROLLERS;controller++) {
        set_pos(controller*CONTOLLER_WIDTH,0);
        for(px=0;px<CONTOLLER_WIDTH*SCREEN_HEIGHT/8;px++)
            display_set_data(controller,0);
    }
    */
    for(int page=0;page<8;page++) {
        display_set_page(0,page);
        display_set_page(1,page);
        display_set_addr(0,0);
        display_set_addr(1,0);
        for(int x=0;x<64;x++) {
            display_set_data(0,0);
            display_set_data(1,0);
        }
    }
}
