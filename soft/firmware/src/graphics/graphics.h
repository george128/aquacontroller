#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "font5x8.h"

#define CONTOLLER_WIDTH 64
#define NUM_CONTROLLERS 2

#define SCREEN_WIDTH CONTOLLER_WIDTH*NUM_CONTROLLERS
#define SCREEN_HEIGHT 64

#define SCREEN_MAX_LINES SCREEN_HEIGHT/FONT_HEIGH

#define LINE_HEIGHT FONT_HEIGH
#define CHAR_WIDTH (FONT_WIDTH+1)
void graphics_init();

#ifdef GRAPHICS_DEBUG
void graphics_test();
#endif

void graphics_flush_auto(char is_flush);

void set_pos(char x, char y);
void draw_pixel(char x, char y, char is_set);
void draw_line(char x1, char y1, char x2, char y2);
void draw_char(char x, char y, const char c);
void draw_text(char x, char y, const char *text, char is_wrap, char is_progmem);
void draw_pic(char x, char y, char width, char height, char *data);
void clear_screen();
void flush_screen();
void invert_region(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2);

#define right_align_x(len) (SCREEN_WIDTH-CHAR_WIDTH*(len)-1)
#define center_align_x(len) ((SCREEN_WIDTH-CHAR_WIDTH*(len))/2-1)
#define size_x(len) (CHAR_WIDTH*(len))
#define line_y(line) (LINE_HEIGHT*(line))
#define bottom_line_y() (LINE_HEIGHT*(SCREEN_MAX_LINES-1))
#endif /*_GRAPHICS_H_*/
