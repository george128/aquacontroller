#ifndef _TIMER_H_
#define _TIMER_H_

struct timer
{
    /*timer period, ms*/
    unsigned short period;
    void (*callback)(unsigned char timer_id, void *data);
    unsigned short last_time;
    void *data;
};

unsigned char timer_setup(struct timer *t);

struct timer *timer_get(unsigned char timer_id);

void timer_free(unsigned char timer_id);

void timer_task();

#endif /*_TIMER_H_*/
