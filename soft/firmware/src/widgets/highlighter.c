#include <graphics/graphics.h>
#include <utils/sleep.h>
#include <platform/utils.h>

#include "highlighter.h"

void highlighter_set(struct dim *dimention)
{
    invert_region(dimention->pos.x,
                  dimention->pos.y,
                  dimention->pos.x+dimention->size.w,
                  dimention->pos.y+dimention->size.h);
}
void highlighter_move_x(struct dim *dim_orig, unsigned char new_x)
{
    unsigned char x;
    for(x=dim_orig->pos.x;x<new_x;x++) {
        sleep_ms100(1);
        invert_region(x,
                      dim_orig->pos.y,
                      x+1,
                      dim_orig->pos.y+dim_orig->size.h);
        invert_region(x+dim_orig->size.w,
                      dim_orig->pos.y,
                      x+dim_orig->size.w+1,
                      dim_orig->pos.y+dim_orig->size.h);
        flush_screen();
    }
}
void highlighter_move_y(struct dim *dim_orig, unsigned char new_y)
{
    unsigned char y=dim_orig->pos.y;
    if(y<new_y) {
        for(;y<new_y;y+=2) {
            sleep_ms100(1);
            invert_region(dim_orig->pos.x,
                          y,
                          dim_orig->pos.x+dim_orig->size.w,
                          y+2);
            invert_region(dim_orig->pos.x,
                          y+dim_orig->size.h,
                          dim_orig->pos.x+dim_orig->size.w,
                          y+dim_orig->size.h+2);
            flush_screen();
        }
    } else { 
        for(;y>new_y;y-=2) {
            sleep_ms100(1);
            invert_region(dim_orig->pos.x,
                          y+dim_orig->size.h-2,
                          dim_orig->pos.x+dim_orig->size.w,
                          y+dim_orig->size.h);
            invert_region(dim_orig->pos.x,
                          y-2,
                          dim_orig->pos.x+dim_orig->size.w,
                          y);
            flush_screen();
        }
    }
}
