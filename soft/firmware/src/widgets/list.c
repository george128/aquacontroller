#include <graphics/graphics.h>

#include "list.h"

void g_list_init(struct g_list *data, unsigned char start_element) {
    data->current = start_element;
    data->highlighter_dim.pos.x=data->x_start;
    data->highlighter_dim.pos.y=data->y_start+start_element*LINE_HEIGHT;
    data->highlighter_dim.size.w=data->width;
    data->highlighter_dim.size.h=LINE_HEIGHT;
    highlighter_set(&data->highlighter_dim);
    flush_screen();
}

void g_list_up(struct g_list *data) {
    if(data->current != 0) {
        data->current--;
        highlighter_move_y(&data->highlighter_dim,
                           data->highlighter_dim.pos.y-LINE_HEIGHT);
        data->highlighter_dim.pos.y-=LINE_HEIGHT;
    }
}

void g_list_down(struct g_list *data) {
    if(data->current+1 < data->num_elements) {
        data->current++;
        highlighter_move_y(&data->highlighter_dim,
                           data->highlighter_dim.pos.y+LINE_HEIGHT);
        data->highlighter_dim.pos.y+=LINE_HEIGHT;
    }
}

unsigned char g_list_get_current(struct g_list *data) {
    return data->current;
}
