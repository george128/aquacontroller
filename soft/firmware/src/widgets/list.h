#ifndef _WIDGETS_LIST_H_
#define _WIDGETS_LIST_H_

#include "highlighter.h"

struct g_list {
    unsigned char num_elements;
    unsigned char current;
    unsigned char is_vert;
    unsigned char width;
    unsigned char y_start;
    unsigned char x_start;
    struct dim highlighter_dim;
};

void g_list_init(struct g_list *data, unsigned char start_element);
void g_list_up(struct g_list *data);
void g_list_down(struct g_list *data);
unsigned char g_list_get_current(struct g_list *data);

#endif /*_WIDGETS_LIST_H_*/
