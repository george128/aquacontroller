#ifndef _SHARED_MEM_H_
#define _SHARED_MEM_H_

/*
shared memory for screens
values are freed on screen destroying
*/

#define SHARED_MEM_SIZE 512

extern char screen_mem[];

#endif /*_SHARED_MEM_H_*/
