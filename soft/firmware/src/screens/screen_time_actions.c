#include <scr_module.h>
#include <graphics.h>
#include <keyboard.h>
#include <strconv.h>
#include <timer.h>
#include <platform/utils.h>
#include <shared_mem.h>

#include "screen_time_actions.h"

#define START_CNT_ADDR 0


static void redraw_screen()
{
    unsigned char i;

    for(i=0;i<SCREEN_MAX_LINES;i++) {
        

    }
}

static void screen_time_actions_display() {
    screen_mem[START_CNT_ADDR]=0;
    

}

static void screen_time_actions_key_press(char keycode)
{
    switch(keycode) {
        case KEY_UP:
            break;
        case KEY_DOWN:
            break;
    }


}

PROGMEM struct screen screen_time_actions=
{
    .display=screen_time_actions_display,
    .destroy=0,
    .key_press=screen_time_actions_key_press
};
