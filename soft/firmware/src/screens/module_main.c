#include <screens/module_main.h>
#include <screens/screen_ban.h>
#include <screens/screen_temperature.h>
#include <screens/screen_pressure.h>
#include <platform/utils.h>
#include <console/console.h>
#include <utils/strings.h>

static char cnt;

static PGM_P const screens_arr[] PROGMEM = {
    (PGM_P)&screen_ban,
    (PGM_P)&screen_temperature,
    (PGM_P)&screen_pressure
};

static void module_main_init()
{
    cnt=1;
    debug_puts("module_main_init()\n");
}

static void module_main_process_event(char event_id)
{
    switch(event_id) {
        case PUBLIC_EVENT_START:
            module_set_screen(SCREEN_BAN);
            break;
        case PUBLIC_EVENT_DONE:
            module_set_screen(SCREEN_BAN);
            break;
        case PRIVATE_EVENT_SCREEN_TEMPERATURE:
            module_set_screen(SCREEN_TEMPERATURE);
            break;
        case PRIVATE_EVENT_SCREEN_PRESSURE:
            module_set_screen(SCREEN_PRESSURE);
            break;
        default:
            debug_print("module_main_process_event: "\
                        "unknown event %d\n",event_id);
    }
}

static const struct screen *module_main_search_screen(char screen_id)
{
    if(screen_id > MAX_SCREEN || screen_id < 0) {
        console_put_msg(STR_SCR_MODULE_MAIN_UNKNOWN_SCREEN, screen_id);
        return NULL;
    } else {
        struct screen *scr;
        scr=(struct screen *)pgm_read_word(&screens_arr[screen_id]);
        debug_print("screen id=%d addr=%p\n", screen_id, (void *)scr);
        console_puts_P(STR_SCR_MODULE_MAIN_SCREEN_ID);
        console_putd(screen_id);
        console_puts_P(STR_SCR_MODULE_MAIN_SCREEN_ADDR);
        console_putmem(&scr, 4);
        console_next_line();
        return scr;
    }
}

static void module_main_set_data(char data_id, void *ptr)
{
    debug_print("module_main_set_data id=%d ptr=%p\n",
                data_id,ptr);
    switch(data_id) {
        case SCREEN_BAN_EVENT_DATA:
            cnt=*(char *)ptr;
            break;
        default:
            debug_print("module_main_set_data: "\
                        "unknown data_id %d\n",
                        data_id);
    }
}

static void *module_main_get_data(char data_id)
{
    debug_print("module_main_get_data id=%d\n",
                data_id);
    switch(data_id) {
        case SCREEN_BAN_EVENT_DATA:
            return &cnt;
        default:
            debug_print("module_main_get_data: "\
                        "unknown data_id %d\n",
                        data_id);
    }
    return NULL;
}

PROGMEM const struct module module_main=
{
    .init=module_main_init,
    .process_event=module_main_process_event,
    .search_screen=module_main_search_screen,
    .get_data=module_main_get_data,
    .set_data=module_main_set_data,
};
