#include <screens/screen_temperature.h>
#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <graphics/graphics.h>
#include <widgets/list.h>
#include <keyboard/keyboard.h>
#include <utils/strings.h>
#include <utils/strconv.h>
#include <timer/timer.h>
#include <platform/utils.h>
#include <console/console.h>
#include <onewire/onewire.h>
#include <onewire/ds18b20.h>

#define MAX_DEVS 5

#define num_devs ((unsigned char *)screen_mem)
#define addr1 1
#define devs ((struct onewire_dev *)(screen_mem+addr1))
#define addr2 addr1+(sizeof(struct onewire_dev)*MAX_DEVS)
#define list_data ((struct g_list *)(screen_mem+addr2))
#define addr3 addr2+sizeof(struct g_list)
#define dev_sel &screen_mem[addr3]
#define addr4 addr3+1
#define dev_proc ((struct ds18b20_proc *)(screen_mem+addr4))
#define addr5 addr4+sizeof(struct ds18b20_proc)
#define timer_tempr_id ((unsigned char *)(screen_mem+addr5))
#define addr6 addr5+1
#define dev_mem ((struct ds18b20_mem *)(screen_mem+addr6))
#define addr7 addr6+sizeof(struct ds18b20_mem)
#define timer_tempr ((struct timer *) (screen_mem+addr7))

static void temperature_callback(struct ds18b20_conf *conf) {
    char buf[16];
    char *ptr=buf;
    debug_print("Temperature=%c%d.%d\n",conf->mem->temperature.sign?'-':'+',
                conf->mem->temperature.degree,
                conf->mem->temperature.fract);
    *(ptr++)=byte2char(*dev_sel);
    *(ptr++)=':';
    if(conf->mem->temperature.sign)
        *(ptr++)='-';
    ptr=byte2dec(conf->mem->temperature.degree, ptr);
    *(ptr++)='.';
    ptr=byte2dec(conf->mem->temperature.fract, ptr);
    *(ptr++)='C';
    *(ptr++)=' ';
    *(ptr++)=0;
    draw_text(size_x(2),line_y(6),buf,0,0);
    flush_screen();
}

static void timer_tmpr_cleanup() {
    if(*dev_sel>=0) {
        *dev_sel=-1;
        timer_free(*timer_tempr_id);
    }
}

static void timer_callback(unsigned char timer_id, void *data) {
    dev_proc->conf.rom=&devs[*dev_sel];
    ds18b20_start_temp(dev_proc, 0);
}

static void screen_temperature_display() 
{
    int i, j;
    char buf[3];

    buf[0]=0;
    clear_screen();
    debug_puts("screen_temperature_display()\n");
    draw_text(0,bottom_line_y(),STR_SCREEN_TEMP_HEAD,0,1);
    *num_devs=onewire_search_rom(devs, MAX_DEVS);
    debug_print("num_devs: %d\n",*num_devs);
    draw_text(center_align_x(15),line_y(0),STR_SCREEN_TEMP_NUMDEVS,0,1);
    draw_char(center_align_x(15)+size_x(14),line_y(0),byte2char(*num_devs));
    for(i=0;i<*num_devs;i++) {
        debug_print("%d: crc=%02x ",i, devs[i].crc);
        draw_char(0,line_y(i+1),byte2char(i));
        //draw_text(size_x(1), line_y(i+1),STR_SCREEN_TEMP_CRC,0,1);
        draw_char(size_x(1),line_y(i+1),':');
        draw_text(size_x(2),line_y(i+1),byte2str(devs[i].crc,buf),0,0);
        draw_char(size_x(4),line_y(i+1),':');
        for(j=0;j<6;j++) {
    	    debug_print("%02x%c", devs[i].serial[j], j!=5?':':' ');
            draw_text(size_x(1+4+j*2),line_y(i+1),byte2str(devs[i].serial[j],buf),0,0);
            //draw_char(size_x(1+5+2), line_y(i+1),':');
        }
        debug_print("family=%02x\n", devs[i].family);
        draw_char(size_x(3+2*7),line_y(i+1),':');
        draw_text(size_x(3+2*7+1),line_y(i+1),byte2str(devs[i].family,buf),0,0);
    }
    *dev_sel=-1;
    list_data->num_elements=*num_devs;
    list_data->width=128;
    list_data->y_start=line_y(1);
    list_data->x_start=0;
    if(*num_devs == 0) {
        return;
    }
    g_list_init(list_data, 0);
    dev_proc->in_process=0;
    dev_proc->callback=temperature_callback;
    dev_proc->conf.mem=dev_mem;
}

static void screen_temperature_destroy()
{
    debug_puts("screen_temperature_destroy()\n");
    timer_tmpr_cleanup();
}

static void screen_temperature_key_press(char keycode)
{
    if(*num_devs == 0 && keycode != KEY_8)
        return;

    switch(keycode) {
        case KEY_1:
            g_list_up(list_data);
            break;
        case KEY_2:
            g_list_down(list_data);
            break;
        case KEY_3:
            if(*dev_sel<0) {
                timer_tempr->period=3000;
                timer_tempr->callback=timer_callback;
                *timer_tempr_id=timer_setup(timer_tempr);
            }
            *dev_sel=(char)g_list_get_current(list_data);
            break;
        case KEY_4:
            timer_tmpr_cleanup();
            break;
        case KEY_8:
            module_process_event(PUBLIC_EVENT_DONE);
            break;
    }
}

PROGMEM const struct screen screen_temperature=
{
    .display=screen_temperature_display,
    .destroy=screen_temperature_destroy,
    .key_press=screen_temperature_key_press
};
