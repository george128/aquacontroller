#ifndef _SCREEN_H_
#define _SCREEN_H_

struct screen
{
    void (*display)();
    void (*destroy)();
    void (*key_press)(char keycode);
};

#endif /*_SCREEN_H_*/
