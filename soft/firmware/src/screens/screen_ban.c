#include <screens/screen_ban.h>
#include <screens/scr_module.h>
#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <utils/strings.h>
#include <utils/strconv.h>
#include <timer/timer.h>
#include <platform/utils.h>
#include <console/console.h>
#include <platform/trigger.h>
#include <rtc/rtc.h>
#include <screens/shared_mem.h>
#include <disk/ff.h>

#include "module_main.h"

static const char str_key_1[] PROGMEM = "KEY_1";
static const char str_key_2[] PROGMEM = "KEY_2";
static const char str_key_3[] PROGMEM = "KEY_3";
static const char str_key_4[] PROGMEM = "KEY_4";
static const char str_key_5[] PROGMEM = "KEY_5";
static const char str_key_6[] PROGMEM = "KEY_6";
static const char str_key_7[] PROGMEM = "KEY_7";
static const char str_key_8[] PROGMEM = "KEY_8";

static PGM_P const keys[8] PROGMEM =
{
    str_key_1,
    str_key_2,
    str_key_3,
    str_key_4,
    str_key_5,
    str_key_6,
    str_key_7,
    str_key_8
};

static const char file_name[] PROGMEM = "test.txt";

#define ADDR_CNT 0
#define ADDR_TIMER_ID 1
#define ADDR_POS 2
#define ADDR_STEP 3
#define ADDR_TIMER 4

#define pos screen_mem[ADDR_POS]
#define step screen_mem[ADDR_STEP]
#define cnt screen_mem[ADDR_CNT]
#define regular_timer_id screen_mem[ADDR_TIMER_ID]
#define t ((struct timer *)(screen_mem+ADDR_TIMER))

static inline void show_cnt()
{
    char buf[3];
    draw_text(54,0,byte2str(cnt,buf),0,0);
    flush_screen();
}

static void show_date()
{
    struct rtc rtc;
    char buf[18];

    rtc_get(&rtc);
    buf[0]=byte2char(rtc.day10);
    buf[1]=byte2char(rtc.day);
    buf[2]='/';
    buf[3]=byte2char(rtc.month10);
    buf[4]=byte2char(rtc.month);
    buf[5]='/';
    buf[6]=byte2char(rtc.year10);
    buf[7]=byte2char(rtc.year);
    buf[8]=' ';
    buf[9]=byte2char(rtc.hour10);
    buf[10]=byte2char(rtc.hour);
    buf[11]=':';
    buf[12]=byte2char(rtc.min10);
    buf[13]=byte2char(rtc.min);
    buf[14]=':';
    buf[15]=byte2char(rtc.sec10);
    buf[16]=byte2char(rtc.sec);
    buf[17]=0;
    draw_text(center_align_x(17),line_y(4),buf,0,0);
    //    trigger_set_all(1<<(rtc.sec));
}

static void animate()
{

    draw_text(0,line_y(3),STR_SPACES,0,1);
    draw_text(pos,line_y(3),STR_HELLO,0,1);
    flush_screen();
    pos+=step;
    if(pos==70 || pos==0)
	step=-step;
}

static int read_file(const char *fname, char *buf, int maxlen)
{
    FATFS fatfs;
    FIL fil;
    UINT rb = -1;

    fatfs.drv=0; /*drive 0*/
    FRESULT res=f_mount(0, &fatfs);
    if(res != FR_OK)
        return -1;
    res=f_open(&fil,fname, FA_READ);
    if(res != FR_OK) 
        goto l_mount;
    res=f_read(&fil,buf,maxlen,&rb);
    res=f_close(&fil);
 l_mount:
    res=f_mount(0, NULL); // umount

    return rb;
}

static void screen_ban_timer_callback(unsigned char timer_id, void *unused)
{
    animate();
    show_date();
}

static const char test_str[] PROGMEM = "\x80\x80\n\x80\x80";

static void screen_ban_timer_callback_start(unsigned char timer_id, void *unused)
{
    char buf[32];
    char fname[13];
    timer_free(timer_id);

    clear_screen();
    draw_text(right_align_x(5), bottom_line_y(), STR_CNT_PP, 0, 1);
    cnt++;
    pos=0;step=1;
    
    int rb=read_file(strcpy_P(fname, file_name), buf, 31);
    if(rb<=0) {
        draw_text(5, line_y(1), STR_FILE_ERR, 0, 1);
    } else {
        buf[rb]=0;
        draw_text(5, line_y(0), buf, 1, 0);
    }
    draw_text(0,line_y(5),test_str,1,1);

    t->period=250;
    t->callback=screen_ban_timer_callback;
    regular_timer_id=timer_setup(t);
    debug_print("screen_ban_display() regular timer_id=%d\n",regular_timer_id);
}

static void screen_ban_display() 
{
    trigger_set_all(0);
    clear_screen();
    cnt=*(char *)module_get_data(SCREEN_BAN_EVENT_DATA);
    debug_print("screen_ban_display() cnt=%d\n",cnt);
    show_cnt();

    t->period=1000;
    t->callback=screen_ban_timer_callback_start;
    timer_setup(t);
}

static void screen_ban_destroy()
{
    timer_free(regular_timer_id);
}

static void screen_ban_key_press(char keycode)
{
    char buf[16];
    strcpy_P(buf, (PGM_P)pgm_read_word(&(keys[(unsigned)keycode])));
    draw_text(0,bottom_line_y(),STR_SPACES,0,1);
    draw_text(0,bottom_line_y(),buf,0,0);
    debug_print("screen_ban_key_press: code=%d\n",
                keycode);
    if(keycode==KEY_8) {
        module_set_data(SCREEN_BAN_EVENT_DATA,&cnt);
        module_process_event(PUBLIC_EVENT_DONE);
    } else if(keycode==KEY_1) {
        module_process_event(PRIVATE_EVENT_SCREEN_TEMPERATURE);
    } else if(keycode==KEY_2) {
        module_process_event(PRIVATE_EVENT_SCREEN_PRESSURE);
    }
    trigger_set_all(1<<keycode);
}

PROGMEM const struct screen screen_ban=
{
    .display=screen_ban_display,
    .destroy=screen_ban_destroy,
    .key_press=screen_ban_key_press
};
