#include <string.h>

#include <platform/utils.h>
#include <screens/screen.h>
#include <screens/scr_module.h>
#include <console/console.h>
#include <utils/strings.h>

#define TASK_IDLE 0
#define TASK_SET_SCREEN 1
#define TASK_PROCESS_EVENT 2
#define TASK_PROCESS_KEY_PRESS 4


#define MODULES_STACK_SIZE 4

static struct module modules[MODULES_STACK_SIZE];
static struct screen cur_scr;
static char task=TASK_IDLE;
static char event_id;
static char screen_id;
static char keycode;
static unsigned char module_idx;

#define MODULE_DEBUG

#ifdef MODULE_DEBUG
#define module_debug console_put_msg
#else
#define module_debug
#endif

void module_init()
{
    module_idx=255;
    memset(modules,0,sizeof(modules));
    memset(&cur_scr,0,sizeof(cur_scr));
}

void module_set_screen(char _screen_id)
{
    screen_id=_screen_id;
    task|=TASK_SET_SCREEN;
}
void module_process_event(char _event_id)
{
    event_id=_event_id;
    task|=TASK_PROCESS_EVENT;
}

void module_key_press(char _keycode)
{
    keycode=_keycode;
    task|=TASK_PROCESS_KEY_PRESS;
}

void module_pop()
{
    module_idx--;
    if(module_idx==0) {
        console_puts_P(STR_SCR_MODULE_POP);
        return;
    }
}

void module_push(const struct module *module)
{
    
    if(module_idx==MODULES_STACK_SIZE-1) {
        console_puts_P(STR_SCR_MODULE_PUSH);
        return;
    }
    module_idx++;
    memcpy_P(modules+module_idx,module,sizeof(*module));
    modules[module_idx].init();
    module_process_event(PUBLIC_EVENT_START);
}

void module_set_data(char data_id, void *ptr)
{
    if(module_idx==255) {
        console_put_msg(STR_SCR_MODULE_SET_DATA_ERROR, module_idx);
        return;
    }
    if(modules[module_idx].set_data!=0)
        modules[module_idx].set_data(data_id,ptr);
}
void *module_get_data(char data_id)
{
    if(module_idx==255) {
        console_put_msg(STR_SCR_MODULE_GET_DATA_ERROR, module_idx);
        return 0;
    }
    if(modules[module_idx].get_data!=0)
        return modules[module_idx].get_data(data_id);

    return 0;
}

void module_task()
{
    if(!task)
        return;
    if(module_idx==255) {
        console_put_msg(STR_SCR_MODULE_TASK_ERROR, module_idx);
        return;
    }
    module_debug(STR_MODULE_TASK, task);
    if(task&TASK_PROCESS_EVENT) {
        module_debug(STR_MODULE_PROCESS_EVENT, event_id);
        modules[module_idx].process_event(event_id);
        task^=TASK_PROCESS_EVENT;
    }
    if(task&TASK_SET_SCREEN)
    {
        const struct screen *scr=modules[module_idx].search_screen(screen_id);
        console_puts_P(STR_MODULE_SCREEN_ADDR);
        console_putmem(scr, 4);
        console_next_line();
        module_debug(STR_MODULE_SET_SCREEN, screen_id);
        if(cur_scr.destroy!=0)
            cur_scr.destroy();
        if(scr!=0) {
            memcpy_P(&cur_scr,scr,sizeof(cur_scr));
            cur_scr.display();
        } else {
            memset(&cur_scr,0,sizeof(cur_scr));
            console_put_msg(STR_SCR_MODULE_SCREEN_ERROR, screen_id);
        }
        task^=TASK_SET_SCREEN;
    }
    if(task&TASK_PROCESS_KEY_PRESS) {
        module_debug(STR_MODULE_PROCESS_KEY, keycode);
        if(cur_scr.key_press!=0)
            cur_scr.key_press(keycode);
        task^=TASK_PROCESS_KEY_PRESS;
    }
    module_debug(STR_MODULE_TASK_DONE, task);
}
