#include <screens/screen_pressure.h>
#include <screens/scr_module.h>
#include <screens/shared_mem.h>
#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <timer/timer.h>
#include <platform/utils.h>
#include <utils/strings.h>
#include <utils/strconv.h>
#include <console/console.h>
#include <pressure/bmp085.h>

#define timer_press ((struct timer *) (screen_mem+0))
#define addr1 sizeof(struct timer)
#define dev_data ((struct bmp085 *) (screen_mem+addr1))
#define addr2 addr1+sizeof(struct bmp085)
#define timer_press_id ((char *)(screen_mem+addr2))

static void pressure_callback(short temperature, short pressure)
{
    char buf[8], *ptr=buf;
    short tmp;
    debug_print("temperature=%d pressure=%d\n", temperature, pressure);
    clear_screen();
    draw_text(0,line_y(0),STR_SCREEN_PRESSURE_TEMPR,0,1);
    tmp=temperature/10;
    if(tmp!=0) {
        ptr=byte2dec(tmp,ptr);
    }
    *(ptr++)='.';
    ptr=byte2dec(temperature%10, ptr);
    *ptr=0;
    draw_text(size_x(13), line_y(0), buf,0,0);
    
    ptr=buf;
    draw_text(0,line_y(1),STR_SCREEN_PRESSURE_PRESS,0,1);
    tmp=pressure/100;
    if(tmp!=0) {
        ptr=byte2dec(tmp,ptr);
    }
    ptr=byte2dec(pressure%100, ptr);
    *ptr=0;
    draw_text(size_x(10), line_y(1), buf,0,0);
    flush_screen();
}

static void timer_callback(unsigned char timer_id, void *data)
{
    bmp085_measure_start(dev_data);
}

static void screen_pressure_display()
{
    clear_screen();
    draw_text(0,line_y(0),STR_SCREEN_PRESSURE_START,0,1);
    flush_screen();

    dev_data->callback=pressure_callback;
    bmp085_init(dev_data);
    timer_press->period=3000;
    timer_press->callback=timer_callback;
    *timer_press_id=timer_setup(timer_press);
}

static void screen_pressure_destroy()
{
    timer_free(*timer_press_id);
}

static void screen_temperature_key_press(char keycode)
{
    if(keycode==KEY_8) {
        module_process_event(PUBLIC_EVENT_DONE);
    }
}

PROGMEM const struct screen screen_pressure=
{
    .display=screen_pressure_display,
    .destroy=screen_pressure_destroy,
    .key_press=screen_temperature_key_press
};
