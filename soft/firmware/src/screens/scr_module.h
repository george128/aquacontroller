#ifndef _SCR_MODULE_H_
#define _SCR_MODULE_H_

#define PUBLIC_EVENT_START 0
#define PUBLIC_EVENT_DONE 1

#define STATE_INIT 0

struct screen;

struct module
{
    void (*init)();
    void (*process_event)(char event_id);
    const struct screen *(*search_screen)(char screen_id);
    void (*set_data)(char data_id, void *ptr);
    void *(*get_data)(char data_id);
};

void module_init();

void module_set_screen(char screen_id);
void module_process_event(char event_id);
void module_key_press(char keycode);
void module_pop();
void module_push(const struct module *module);

void module_set_data(char data_id, void *ptr);
void *module_get_data(char data_id);

void module_task();

#endif /*_SCR_MODULE_H_*/
