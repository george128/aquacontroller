#ifndef _MODULE_MAIN_H_
#define _MODULE_MAIN_H_

#include <screens/scr_module.h>

#define SCREEN_BAN 0
#define SCREEN_TEMPERATURE 1
#define SCREEN_PRESSURE 2

#define MAX_SCREEN 2

#define PRIVATE_EVENT_SCREEN_TEMPERATURE 10
#define PRIVATE_EVENT_SCREEN_PRESSURE 11

extern const struct module module_main;

#endif /*_MODULE_MAIN_H_*/
