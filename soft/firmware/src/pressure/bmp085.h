#ifndef _BMP_058_H_
#define _BMP_058_H_

#include <timer/timer.h>

#define BMP085_START_TEMPERATURE  0x2e //4.5ms
#define BMP085_START_PRESSURE_ULP 0x34 //ultra low poer 4.5ms
#define BMP085_START_PRESSURE_ST  0x74 //standatd 7.5ms
#define BMP085_START_PRESSURE_HI  0xb4 //high resolution 13.5ms
#define BMP085_START_PRESSURE_UHI 0xf4 //ultra high resolution 25.5ms

#define BMP085_REGISTER_CONTROL            0xf4 //control register
#define BMP085_REGISTER_RESULT             0xf6 //result UT or UP 0xf7 opt. 0xf8
#define BMP085_EEPROM_START                0xaa

struct bmp085_eeprom {
    short ac1;
    short ac2;
    short ac3;
    unsigned short ac4;
    unsigned short ac5;
    unsigned short ac6;
    short b1;
    short b2;
    unsigned short mb;
    short mc;
    short md;
}  __attribute__ ((packed));

struct bmp085 {
    struct bmp085_eeprom eeprom;
    struct timer timer_wait;
    void (*callback)(short temperature, short pressure);
    unsigned short ut;
};
char bmp085_init(struct bmp085 *data);
void bmp085_measure_start(struct bmp085 *data);

#endif /*_BMP_058_H_*/
