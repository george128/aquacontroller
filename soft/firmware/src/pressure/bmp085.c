#include <platform/i2c.h>
#include <platform/utils.h>
#include <console/console.h>
#include <utils/strings.h>
#include <utils/utils.h>
#include <utils/sleep.h>

#include "bmp085.h"

#define BMP085_DEBUG 1

#define OSRS 0

#if BMP085_DEBUG > 0
PROGMEM const static char STR_BMP085_UT[]="bmp085: ut=";
PROGMEM const static char STR_BMP085_UP[]=" up=";
PROGMEM const static char STR_BMP085_T[]="bmp085: t=";
PROGMEM const static char STR_BMP085_P[]="*0.1C p=";
PROGMEM const static char STR_BMP085_PA[]="pa";
PROGMEM const static char STR_BMP085_AC1[]="bmp085: eeprom ac1=";
PROGMEM const static char STR_BMP085_AC2[]="bmp085: eeprom ac2=";
PROGMEM const static char STR_BMP085_AC3[]="bmp085: eeprom ac3=";
PROGMEM const static char STR_BMP085_AC4[]="bmp085: eeprom ac4=";
PROGMEM const static char STR_BMP085_AC5[]="bmp085: eeprom ac5=";
PROGMEM const static char STR_BMP085_AC6[]="bmp085: eeprom ac6=";
PROGMEM const static char STR_BMP085_B1[]="bmp085: eeprom  b1=";
PROGMEM const static char STR_BMP085_B2[]="bmp085: eeprom  b2=";
PROGMEM const static char STR_BMP085_MB[]="bmp085: eeprom  mb=";
PROGMEM const static char STR_BMP085_MC[]="bmp085: eeprom  mc=";
PROGMEM const static char STR_BMP085_MD[]="bmp085: eeprom  md=";
PROGMEM const static char STR_BMP085_TEMP[]="bmp085: temperature=";
PROGMEM const static char STR_BMP085_PRESS[]=" pressure=";
#endif

static void bmp085_cb(unsigned char timer_id, void *data) {
    struct bmp085 *bmp085=(struct bmp085 *)data;
    
    timer_free(timer_id);
    if(bmp085->ut == 0) {
        char value = BMP085_START_PRESSURE_ST;
        unsigned short ut;
        i2c_read(I2C_DEV_NO_BMP085, BMP085_REGISTER_RESULT, 2, (char *)&ut);
        bmp085->ut=swap_ushort(ut);
        i2c_write(I2C_DEV_NO_BMP085, BMP085_REGISTER_CONTROL, 1, &value);
#if BMP085_DEBUG > 0
        console_puts_P(STR_BMP085_UT);
        console_put_short(bmp085->ut);
        console_next_line();
#endif
        bmp085->timer_wait.period = 8;
        timer_setup(&bmp085->timer_wait);
    } else {
        struct bmp085_eeprom *eeprom = &bmp085->eeprom;
        unsigned short up;
        unsigned long b4;
        long x1, x2, x3, b3, b5, b6, b7, p, b6_2;
        short t, press;
    
        i2c_read(I2C_DEV_NO_BMP085, BMP085_REGISTER_RESULT, 2, (char *)&up);
#if BMP085_DEBUG > 0
        up = swap_ushort(up);
        console_puts_P(STR_BMP085_UT);
        console_put_short(bmp085->ut);
        console_put_msg_short(STR_BMP085_UP, up);
#endif
        x1=(((long)bmp085->ut-(long)eeprom->ac6)*(long)eeprom->ac5)>>15;
        long tmp1=x1+eeprom->md;
        x2=(((long)eeprom->mc<<11)-(tmp1>>1))/tmp1;
        debug_print("tmp1=%d\n", eeprom->mc<<11);
        b5=x1+x2;
        debug_print("x1=%ld x2=%ld b5=%ld\n",x1,x2,b5);
        /**/t=(b5+8)>>4;
        b6=b5-4000;
        b6_2=b6*b6>>12;
        x1=(eeprom->b2*b6_2)>>11;
        x2=(eeprom->ac2*b6)>>11;
        x3=x1+x2;
        b3=(((((long)eeprom->ac1)*4+x3)<<OSRS)+2)>>2;
        debug_print("b6=%ld x1=%ld x2=%ld x3=%ld b3=%ld\n",
                    b6,x1,x2,x3,b3);
        x1=(eeprom->ac3*b6)>>13;
        x2=(eeprom->b1*b6_2)>>16;
        x3=((x1+x2)+2)>>2;
        tmp1=(eeprom->ac4*(unsigned long)(x3+32768));
        b4=tmp1>>15;
        b7=((unsigned long)up-b3)*(50000>>OSRS);
        debug_print("x1=%ld x2=%ld x3=%ld ac4=%d tmp1=%ld b4=%ld b7=%ld\n",
                    x1,x2,x3,eeprom->ac4,tmp1,b4,b7);
        if(b7<0x80000000) 
            p=(b7<<1)/b4;
        else
            p=(b7/b4)<<1;
        x1=((p>>1)*(p>>1))>>14;
        debug_print("p=%ld x1=%ld\n",p, x1);
        x1=(x1*3038)>>16;
        x2=(-7357*p)>>16;
        debug_print("x1=%ld x2=%ld\n",x1,x2);
        p=p+((x1+x2+3791)>>4);
        debug_print("temp=%d*0.1C press=%ldpa\n",t,p);
        press=(p*760+(101325>>1))/101325;
#if BMP085_DEBUG > 0
        console_puts_P(STR_BMP085_T);
        console_put_short(t);
        console_puts_P(STR_BMP085_P);
        console_put_long(p);
        console_puts_P(STR_BMP085_PA);
        console_next_line();
#endif
        bmp085->callback(t, press);
    }
}

char bmp085_init(struct bmp085 *data) {
    unsigned char rb;
    struct bmp085_eeprom rdata, *eeprom=&data->eeprom;
    if((rb=i2c_read(I2C_DEV_NO_BMP085, BMP085_EEPROM_START, sizeof(rdata), (char *)&rdata)) != sizeof(rdata)) {
        console_put_msg(STR_BMP085_WRONG_EEPROM_READ, rb);
        return 0;
    }
    eeprom->ac1=swap_short(rdata.ac1);
    eeprom->ac2=swap_short(rdata.ac2);
    eeprom->ac3=swap_short(rdata.ac3);
    eeprom->ac4=swap_ushort(rdata.ac4);
    eeprom->ac5=swap_ushort(rdata.ac5);
    eeprom->ac6=swap_ushort(rdata.ac6);
    eeprom->b1=swap_short(rdata.b1);
    eeprom->b2=swap_short(rdata.b2);
    eeprom->mb=swap_ushort(rdata.mb);
    eeprom->mc=swap_short(rdata.mc);
    eeprom->md=swap_short(rdata.md);
  
#if BMP085_DEBUG > 0
    {
        console_put_msg_short(STR_BMP085_AC1, eeprom->ac1);
        console_put_msg_short(STR_BMP085_AC2, eeprom->ac2);
        console_put_msg_short(STR_BMP085_AC3, eeprom->ac3);
        console_put_msg_short(STR_BMP085_AC4, eeprom->ac4);
        console_put_msg_short(STR_BMP085_AC5, eeprom->ac5);
        console_put_msg_short(STR_BMP085_AC6, eeprom->ac6);
        console_put_msg_short(STR_BMP085_B1, eeprom->b1);
        console_put_msg_short(STR_BMP085_B2, eeprom->b2);
        console_put_msg_short(STR_BMP085_MB, eeprom->mb);
        console_put_msg_short(STR_BMP085_MC, eeprom->mc);
        console_put_msg_short(STR_BMP085_MD, eeprom->md);
    }
#endif
    return 1;
}

void bmp085_measure_start(struct bmp085 *data) {
    char value = BMP085_START_TEMPERATURE;
    data->timer_wait.callback=bmp085_cb;
    data->timer_wait.data = data;
    data->timer_wait.period=5;
    data->ut = 0;
    i2c_write(I2C_DEV_NO_BMP085, BMP085_REGISTER_CONTROL, 1, &value);
    timer_setup(&data->timer_wait);
}

#if BMP085_DEBUG > 0
static char test_done;
#endif

static void bmp085_test_callback(short temperature, short pressure) {
    console_puts_P(STR_BMP085_TEMP);
    console_put_short(temperature);
    console_put_msg_short(STR_BMP085_PRESS, pressure);
    test_done = 1;
}

void bmp085_test() {
    struct bmp085 d={
        .callback = bmp085_test_callback,
    };
    if(!bmp085_init(&d))
        return;
    while(1) {
        test_done=0;
        bmp085_measure_start(&d);
        while(!test_done)
            timer_task();
        sleep(5);
    }
}
