#include <disk/ff.h>
#include <disk/diskio.h>
#include <console/console.h>
#include <platform/utils.h>
#include <utils/strings.h>

static void print_res(const char *str, unsigned char res) {
    console_puts_P(str);
    console_putd(res);
    console_putc('\n');
}

static void print_status() 
{
    print_res(STR_STATUS, disk_status(0));
}

static inline void disk_test1()
{
    unsigned char res;
    char buf[512];

    print_status();
    res=disk_initialize(0);
    print_res(STR_INIT, res);
    print_status();

    res=disk_ioctl(0, MMC_GET_TYPE, buf);
    print_res(STR_MMC_TYPE_RES, res);
    print_res(STR_MMC_TYPE, buf[0]);

    res=disk_ioctl(0, MMC_GET_CSD, buf);
    print_res(STR_MMC_CSD_RES, res);
    console_puts_P(STR_MMC_CSD);
    console_putmem(buf, 16);
    console_putc('\n');

    res=disk_ioctl(0, MMC_GET_CID, buf);
    print_res(STR_MMC_CID_RES, res);
    console_puts_P(STR_MMC_CID);
    console_putmem(buf, 16);
    console_putc('\n');

    res=disk_ioctl(0, MMC_GET_OCR, buf);
    print_res(STR_MMC_OCR_RES, res);
    console_puts_P(STR_MMC_OCR);
    console_putmem(buf, 4);
    console_putc('\n');

    res=disk_read(0, (BYTE *)buf, 2152, 1); /*test.txt contents*/
    buf[511]=0;
    print_res(STR_DISK_READ, res);
    console_puts_P(STR_BUF);
    console_puts(buf);
    console_putc('\'');
    console_putc('\n');

    console_puts_P(STR_DISKIO_TEST_DONE);
}


static inline void disk_test2()
{
    char buf[64];

    FATFS fatfs;
    FIL fil;
    UINT rb;

    fatfs.drv=0; /*drive 0*/
    console_puts_P(STR_DISKIO_TEST2);

    FRESULT res=f_mount(0, &fatfs);
    console_puts_P(STR_MOUNT);
    console_putd(res);
    console_putc('\n');
    print_status();
    if(res!=FR_OK) goto err;
    
    console_puts_P(STR_FATFS);
    console_putmem(&fatfs, (char)sizeof(fatfs));
    console_putc('\n');

    res=f_open(&fil,"/test.txt", FA_READ);
    console_puts_P(STR_OPEN);
    console_putd(res);
    console_puts_P(STR_FLAG);
    console_putd(fil.flag);
    console_putc('\n');
    console_puts_P(STR_FIL);
    console_putmem(&fil, sizeof(fil));
    console_putc('\n');
    print_status();
    if(res!=FR_OK) goto err;

    res=f_read(&fil,buf,64,&rb);
    buf[rb]=0;
    console_puts_P(STR_READ_RES);
    console_putd(res);
    console_putc('\n');
    console_puts_P(STR_RB);
    console_putd(rb);
    console_putc('\n');
    print_status();
    if(res!=FR_OK) goto err;

    console_puts_P(STR_BUF);
    console_puts(buf);
    console_putc('\'');
    console_putc('\n');

    res=f_close(&fil);
    console_puts_P(STR_CLOSE_RES);
    console_putd(res);
    console_putc('\n');
    if(res!=FR_OK) goto err;

    res=f_mount(0, NULL); // umount
    console_puts_P(STR_UMOUNT_RES);
    console_putd(res);
    console_putc('\n');
    if(res!=FR_OK) goto err;

    console_puts_P(STR_DISKIO_TEST2_DONE);
    return;
 err:
    console_puts_P(STR_ERROR);
}

void disk_test()
{
    disk_test2();
}
