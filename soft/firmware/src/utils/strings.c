#include <platform/utils.h>

#include <utils/strings.h>

PROGMEM const char STR_ON[]="on ";
PROGMEM const char STR_OFF[]="off";

PROGMEM const char STR_STARTING[]="Starting...\n";
PROGMEM const char STR_MAIN_LOOP[]="Start main loop\n";

/* screen_ban */
PROGMEM const char STR_SPACES[]="                     ";
PROGMEM const char STR_HELLO[]="Hello!!!";
PROGMEM const char STR_CNT_PP[]="Cnt++";
PROGMEM const char STR_FILE_ERR[]="File error!!!";


/*tests */
PROGMEM const char STR_STATUS[]="status: ";
PROGMEM const char STR_INIT[]="init: ";
PROGMEM const char STR_MMC_TYPE[]="mmc type: ";
PROGMEM const char STR_MMC_TYPE_RES[]="mmc type res: ";
PROGMEM const char STR_MMC_CSD[]="mmc csd: ";
PROGMEM const char STR_MMC_CSD_RES[]="mmc csd res: ";
PROGMEM const char STR_MMC_CID[]="mmc cid: ";
PROGMEM const char STR_MMC_CID_RES[]="mmc cid res: ";
PROGMEM const char STR_MMC_OCR[]="mmc ocr: ";
PROGMEM const char STR_MMC_OCR_RES[]="mmc ocr res: ";
PROGMEM const char STR_DISK_READ[]="disk_read: ";
PROGMEM const char STR_BUF[]="buf: '";
PROGMEM const char STR_DISKIO_TEST_DONE[]="diskio test done\n";
PROGMEM const char STR_DISKIO_TEST2[]="diskio_test2\n";
PROGMEM const char STR_DISKIO_TEST2_DONE[]="diskio_test2 done\n";
PROGMEM const char STR_MOUNT[]="mount: ";
PROGMEM const char STR_FATFS[]="fatfs: ";
PROGMEM const char STR_OPEN[]="open: ";
PROGMEM const char STR_FLAG[]=" flag=";
PROGMEM const char STR_FIL[]="fil: ";
PROGMEM const char STR_READ_RES[]="read: res=";
PROGMEM const char STR_RB[]="rb=";
PROGMEM const char STR_CLOSE_RES[]="close res=";
PROGMEM const char STR_UMOUNT_RES[]="umount res=";
PROGMEM const char STR_SEND_CMD[]="send_cmd ";
PROGMEM const char STR_ARG[]=" arg=";
PROGMEM const char STR_RES[]=" res=";
PROGMEM const char STR_SYSTIME[]="systime=";
PROGMEM const char STR_NOW_SYSTIME[]=" now systime=";
PROGMEM const char STR_NOW_SYSTIME2[]="now systime2=";
PROGMEM const char STR_DISK_READ_SECTOR[]="disk_read sector=";
PROGMEM const char STR_COUNT[]=" count=0x";
PROGMEM const char STR_DISK_READ_RES[]="disk_read res count=0x";
PROGMEM const char STR_OPEN_DIR[]="f_open dir=";
PROGMEM const char STR_1307[]="1307: ";
PROGMEM const char STR_ERROR[]="Error!!!\n";
PROGMEM const char STR_CARD_TYPE[]="CardType: ";

PROGMEM const char STR_MODULE_TASK[]="module_task: task=";
PROGMEM const char STR_MODULE_TASK_DONE[]="module_task: done task=";
PROGMEM const char STR_MODULE_PROCESS_EVENT[]="module_task: process_event ";
PROGMEM const char STR_MODULE_SET_SCREEN[]="module_task: set_screen ";
PROGMEM const char STR_MODULE_PROCESS_KEY[]="module_task: process_key_press ";
PROGMEM const char STR_MODULE_SCREEN_ADDR[]="module_task: screen addr=";

PROGMEM const char STR_SCR_MODULE_MAIN_SCREEN_ID[]="screen id=";
PROGMEM const char STR_SCR_MODULE_MAIN_SCREEN_ADDR[]=" addr=";

PROGMEM const char STR_TIMER_CALLBACK[]="timer callback ";
PROGMEM const char STR_TIMER_SETUP[]="timer setup: ";
PROGMEM const char STR_TIMER_FREE[]="timer free: ";

PROGMEM const char STR_ONEWIRE_RESET[]="1-wire reset: ";
PROGMEM const char STR_ONEWIRE_WRITE[]="1-wire wb=";
PROGMEM const char STR_ONEWIRE_READ[]="1-wire rb=";
PROGMEM const char STR_ONEWIRE_STABLE_0[]="1-wire search: stable 0 i=";
PROGMEM const char STR_ONEWIRE_STABLE_1[]="1-wire search: stable 1 i=";
PROGMEM const char STR_ONEWIRE_LAST_DIFF_1[]="1-wire search: last diff 1 i=";
PROGMEM const char STR_ONEWIRE_NEW_DIFF[]="1-wire search: new diff=";
PROGMEM const char STR_ONEWIRE_NEW_DIFF2[]=" i=";
PROGMEM const char STR_ONEWIRE_SEARCH_FAILED[]="1-wire search: failed i=";
PROGMEM const char STR_ONEWIRE_SEARCH_ROM_BYTE1[]="1-wire search: rom_byte[";
PROGMEM const char STR_ONEWIRE_SEARCH_ROM_BYTE2[]="]=";
PROGMEM const char STR_ONEWIRE_SEARCH_ROM_I1[]="1-wire search: i=";
PROGMEM const char STR_ONEWIRE_SEARCH_ROM_I2[]=" rom_byte=";
PROGMEM const char STR_ONEWIRE_SEARCH_ROM_I3[]=" mask=";

/* end tests */

PROGMEM const char STR_INIT_DISK[]="Initializing disk...\n";
PROGMEM const char STR_INIT_DISPLAY[]="Initializing display...\n";
PROGMEM const char STR_INIT_KEYBOARD[]="Initializing keyboard...\n";
PROGMEM const char STR_TRIGGER_INIT[]="Initializing triggers...\n";
PROGMEM const char STR_CONSOLE_INIT1[]="console: registered ";
PROGMEM const char STR_CONSOLE_INIT2[]=" console(s)\n";
PROGMEM const char STR_MAIN_CONSOLE[]="console: '";

PROGMEM const char STR_SCREEN_TEMP_HEAD[]="Press Key_8 to exit";
PROGMEM const char STR_SCREEN_TEMP_NUMDEVS[]="Found devices:";
PROGMEM const char STR_SCREEN_PRESSURE_START[]="Starting conversion";
PROGMEM const char STR_SCREEN_PRESSURE_TEMPR[]="Temperature: ";
PROGMEM const char STR_SCREEN_PRESSURE_PRESS[]="Pressure: ";

PROGMEM const char STR_SCR_MODULE_PUSH[]="module_push: stack overflow!!!\n";
PROGMEM const char STR_SCR_MODULE_POP[]="podule_pop: stack underflow!!!\n";
PROGMEM const char STR_SCR_MODULE_SET_DATA_ERROR[]="module_set_data error: idx=";
PROGMEM const char STR_SCR_MODULE_GET_DATA_ERROR[]="module_get_data error: idx=";
PROGMEM const char STR_SCR_MODULE_TASK_ERROR[]="module_task error: idx=";
PROGMEM const char STR_SCR_MODULE_SCREEN_ERROR[]="module_task: can't set screen: ";

PROGMEM const char STR_SCR_MODULE_MAIN_UNKNOWN_SCREEN[]="module_main_search_screen: unknown screen_id ";

PROGMEM const char STR_TIMER_NO_SLOTS[]="No free slot for new timer!!!\n";
PROGMEM const char STR_TIMER_WRONG_ID[]="timer_free: wrong timer_id ";

PROGMEM const char STR_ONEWIRE_NO_PRESENCE[]="No any 1-wire device present\n";

PROGMEM const char STR_BMP085_WRONG_EEPROM_READ[]="bmp085: unsuccessfull read eeprom rb=";
