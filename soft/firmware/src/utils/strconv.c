#include <utils/strconv.h>
#include <platform/utils.h>

static const char PROGMEM hex[]="0123456789ABCDEF";

char* byte2str(char byte, char *str)
{
    str[2]=0;
    str[0]=	read_rom(hex+((byte>>4)&0xf));
    str[1]= read_rom(hex+(byte&0xf));
    return str;
}

char str2byte(char *str) {
    char b1=-1,b2=-1;
    int i;
    for(i=0;i<16;i++) {
        char ch=read_rom(hex+i);
        if(ch==str[0])
            b1=i;
        if(ch==str[1])
            b2=i;
    }
    return b1*16+b2;
}

char byte2char(char byte) {
    return read_rom(hex+byte);
}

char* byte2dec(char byte, char *str) {
    char d=byte/10;
    if(d)
        *(str++)=byte2char(d);
    *(str++)=byte2char(byte%10);
    return str;
}
