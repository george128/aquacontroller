#ifndef _STRINGS_H_
#define _STRINGS_H_

extern const char STR_ON[];
extern const char STR_OFF[];

extern const char STR_STARTING[];
extern const char STR_MAIN_LOOP[];

/* screen_ban */
extern const char STR_SPACES[];
extern const char STR_HELLO[];
extern const char STR_CNT_PP[];
extern const char STR_FILE_ERR[];

/*tests */
extern const char STR_STATUS[];
extern const char STR_INIT[];
extern const char STR_MMC_TYPE[];
extern const char STR_MMC_TYPE_RES[];
extern const char STR_MMC_CSD[];
extern const char STR_MMC_CSD_RES[];
extern const char STR_MMC_CID[];
extern const char STR_MMC_CID_RES[];
extern const char STR_MMC_OCR[];
extern const char STR_MMC_OCR_RES[];
extern const char STR_DISK_READ[];
extern const char STR_BUF[];
extern const char STR_DISKIO_TEST_DONE[];
extern const char STR_DISKIO_TEST2[];
extern const char STR_DISKIO_TEST2_DONE[];
extern const char STR_MOUNT[];
extern const char STR_FATFS[];
extern const char STR_OPEN[];
extern const char STR_FLAG[];
extern const char STR_FIL[];
extern const char STR_READ_RES[];
extern const char STR_RB[];
extern const char STR_CLOSE_RES[];
extern const char STR_UMOUNT_RES[];
extern const char STR_SEND_CMD[];
extern const char STR_ARG[];
extern const char STR_RES[];
extern const char STR_SYSTIME[];
extern const char STR_NOW_SYSTIME[];
extern const char STR_NOW_SYSTIME2[];
extern const char STR_DISK_READ_SECTOR[];
extern const char STR_COUNT[];
extern const char STR_DISK_READ_RES[];
extern const char STR_OPEN_DIR[];
extern const char STR_1307[];
extern const char STR_ERROR[];
extern const char STR_CARD_TYPE[];

extern const char STR_MODULE_TASK[];
extern const char STR_MODULE_TASK_DONE[];
extern const char STR_MODULE_PROCESS_EVENT[];
extern const char STR_MODULE_SET_SCREEN[];
extern const char STR_MODULE_PROCESS_KEY[];
extern const char STR_MODULE_SCREEN_ADDR[];

extern const char STR_SCR_MODULE_MAIN_SCREEN_ID[];
extern const char STR_SCR_MODULE_MAIN_SCREEN_ADDR[];

extern const char STR_TIMER_CALLBACK[];
extern const char STR_TIMER_SETUP[];
extern const char STR_TIMER_FREE[];

extern const char STR_ONEWIRE_RESET[];
extern const char STR_ONEWIRE_WRITE[];
extern const char STR_ONEWIRE_READ[];
extern const char STR_ONEWIRE_STABLE_0[];
extern const char STR_ONEWIRE_STABLE_1[];
extern const char STR_ONEWIRE_LAST_DIFF_1[];
extern const char STR_ONEWIRE_NEW_DIFF[];
extern const char STR_ONEWIRE_NEW_DIFF2[];
extern const char STR_ONEWIRE_SEARCH_FAILED[];
extern const char STR_ONEWIRE_SEARCH_ROM_BYTE1[];
extern const char STR_ONEWIRE_SEARCH_ROM_BYTE2[];
extern const char STR_ONEWIRE_SEARCH_ROM_I1[];
extern const char STR_ONEWIRE_SEARCH_ROM_I2[];
extern const char STR_ONEWIRE_SEARCH_ROM_I3[];

/* end tests */

extern const char STR_INIT_DISK[];
extern const char STR_INIT_DISPLAY[];
extern const char STR_INIT_KEYBOARD[];

extern const char STR_TRIGGER_INIT[];
extern const char STR_CONSOLE_INIT1[];
extern const char STR_CONSOLE_INIT2[];

extern const char STR_MAIN_CONSOLE[];

extern const char STR_SCREEN_TEMP_HEAD[];
extern const char STR_SCREEN_TEMP_NUMDEVS[];

extern const char STR_SCREEN_PRESSURE_START[];
extern const char STR_SCREEN_PRESSURE_TEMPR[];
extern const char STR_SCREEN_PRESSURE_PRESS[];

extern const char STR_SCR_MODULE_PUSH[];
extern const char STR_SCR_MODULE_POP[];
extern const char STR_SCR_MODULE_SET_DATA_ERROR[];
extern const char STR_SCR_MODULE_GET_DATA_ERROR[];
extern const char STR_SCR_MODULE_TASK_ERROR[];
extern const char STR_SCR_MODULE_SCREEN_ERROR[];

extern const char STR_SCR_MODULE_MAIN_UNKNOWN_SCREEN[];

extern const char STR_TIMER_NO_SLOTS[];
extern const char STR_TIMER_WRONG_ID[];

extern const char STR_ONEWIRE_NO_PRESENCE[];

extern const char STR_BMP085_WRONG_EEPROM_READ[];

#endif /*_STRINGS_H_*/
