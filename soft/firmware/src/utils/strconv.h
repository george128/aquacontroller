#ifndef _STRCONV_H_
#define _STRCONV_H_

char* byte2str(char byte, char *str);
char str2byte(char *str);

char byte2char(char byte);
char* byte2dec(char byte, char *str);
#endif /*_STRCONV_H_*/

