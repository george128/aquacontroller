#include "utils.h"

union swap16{
    struct {
    char b1, b2;
    } b;
    unsigned short us;
    short s;
};

short swap_short(short val)
{
    union swap16 m = {
        .s = val
    };
    char tmp;
    tmp=m.b.b1;
    m.b.b1 = m.b.b2;
    m.b.b2 = tmp;
    return m.s;
}

unsigned short swap_ushort(unsigned short val)
{
    union swap16 m = {
        .us = val
    };
    char tmp;
    tmp=m.b.b1;
    m.b.b1 = m.b.b2;
    m.b.b2 = tmp;
    return m.us;
}
