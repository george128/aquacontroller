#ifndef _ONEWIRE_H_
#define _ONEWIRE_H_

#define ONEWIRE_SEARCHROM 0xF0
#define ONEWIRE_READROM   0x33
#define ONEWIRE_MATCHROM  0x55
#define ONEWIRE_SKIPROM   0xCC

#define ONEWIRE_SEARCH_FAILED 0xff
#define ONEWIRE_SEARCH_COMPLETE 0

#define ONEWIRE_MASK(n) (1<<(n))
#define ONEWIRE_MASK_NEXT(n) n<<=1

struct onewire_dev
{
    unsigned char family;
    unsigned char serial[6];
    unsigned char crc;
} __attribute__ ((packed));

unsigned char onewire_reset();

unsigned char onewire_search_rom(struct onewire_dev *devs, unsigned char max_devs);
unsigned char onewire_match_rom(struct onewire_dev *rom);
unsigned char onewire_skip_rom();
struct onewire_dev* onewire_read_rom(struct onewire_dev *rom);

void onewire_write_byte(unsigned char byte);
unsigned char onewire_read_byte();

#endif /*_ONEWIRE_H_*/
