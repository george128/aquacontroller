#ifndef _DS18B20_H_
#define _DS18B20_H_

#include <timer/timer.h>

/*Start temperature conversion. get 1 byte of status*/
#define DS18B20_CONVERT 0x44
/*read 1 byte of power status*/
#define DS18B20_READ_POWER 0xb4

/*write user, conf to RAM. send 3 bytes*/
#define DS18B20_WRITE_SCRATCH 0x4e
/*read up to 9 bytes*/
#define DS18B20_READ_SCRATCH 0xbe
/*store user, conf*/
#define DS18B20_STORE_SCRATCH 0x48
/*load user, conf from EEPROM to RAM. get 1 byte status*/
#define DS18B20_LOAD_SCRATCH 0xb8

struct ds18b20_temp {
    unsigned int fract:4;
    unsigned int degree:7;
    unsigned int sign:5;
}  __attribute__ ((packed));

struct ds18b20_mem {
    struct ds18b20_temp temperature;
    unsigned short user;
    struct {
        unsigned int one:5;
        unsigned int resolution:2;
        unsigned int zero:1;
    } __attribute__ ((packed)) conf;
    unsigned char reserved1;
    unsigned char reserved2;
    unsigned char reserved3;
    unsigned char crc;
}  __attribute__ ((packed));


struct ds18b20_conf {
    struct ds18b20_mem *mem;
    struct onewire_dev *rom;
};

struct ds18b20_proc {
    struct ds18b20_conf conf;
    struct timer timer_wait;
    void (*callback)(struct ds18b20_conf *conf);
    char in_process;
};

unsigned char ds18b20_read_conf(struct ds18b20_conf *conf);
unsigned char ds18b20_start_temp(struct ds18b20_proc *proc, char is_write_conf);

#endif /*_DS18B20_H_*/
