#include <platform/delay.h>
#include <platform/utils.h>

#include "onewire.h"
#include "ds18b20.h"


#define CHECK_RESULT(result) if(!result) return result;

static unsigned char select_dev(struct onewire_dev *rom) {
    unsigned char ret;
    if(!rom) {
        ret=onewire_skip_rom();
    } else {
        ret=onewire_match_rom(rom);
    }
    return ret;
}

static const unsigned short PROGMEM ds18b20_delays[]={100,200,400,800};

static unsigned short ds18b20_calculate_delay(struct ds18b20_conf *conf) {
    char resolution=conf?conf->mem->conf.resolution:3;
    if(resolution>=0 && resolution <=3) {
        return pgm_read_word(&ds18b20_delays[resolution]);
    }
    debug_print("ds18b20 wait_conversion: unexpected resolution=%d\n",
                resolution);
    return 0;
}

static void ds18b20_cb(unsigned char timer_id, void *data) {
    struct ds18b20_proc *proc=(struct ds18b20_proc *)data;
    struct ds18b20_conf *conf= &proc->conf;
    timer_free(timer_id);
    if(ds18b20_read_conf(conf)) {
        proc->in_process = 0;
        proc->callback(conf);
    } else {
        proc->in_process = 0;
    }
}

unsigned char ds18b20_read_conf(struct ds18b20_conf *conf) {
    unsigned char res = select_dev(conf->rom);
    CHECK_RESULT(res);
    char *mem=(char *)conf->mem;
    onewire_write_byte(DS18B20_READ_SCRATCH);
    debug_print("sizeof(conf->mem)=%d\n",sizeof(*conf->mem));
    for(char i=0;i<sizeof(*conf->mem);i++,mem++) 
        *mem=onewire_read_byte();
    return 1;
}

unsigned char ds18b20_start_temp(struct ds18b20_proc *proc, char is_write_conf) {
    struct ds18b20_conf *conf = &proc->conf;
    unsigned char ret;
    if(proc->in_process) {
        debug_puts("ds18b20: conversion in process...\n");
        return 0;
    }
    proc->in_process = 1;
    ret = select_dev(conf->rom);
    CHECK_RESULT(ret);
    if(is_write_conf) {
        char *mem=(char *)&conf->mem->user;
        onewire_write_byte(DS18B20_WRITE_SCRATCH);
        for(char i=0;i<3;i++,mem++)
            onewire_write_byte(*mem);
        ret = select_dev(conf->rom);
        CHECK_RESULT(ret);
    }
    onewire_write_byte(DS18B20_CONVERT);
    proc->timer_wait.callback=ds18b20_cb;
    proc->timer_wait.data = proc;
    proc->timer_wait.period=ds18b20_calculate_delay(conf);
    timer_setup(&proc->timer_wait);
    return 0;
}
