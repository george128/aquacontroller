#include <console/console.h>
#include <onewire/onewire.h>
#include <onewire/ds18b20.h>        
#include <timer/timer.h>
#include <platform/utils.h>
#include <string.h>
#include <utils/sleep.h>

PROGMEM static const char TEMP[]="ds18b20 temp: ";
PROGMEM static const char NUM_DEVS[]="num_devs: ";
PROGMEM static const char CRC[]=": crc=";
PROGMEM static const char FAMILY[]=" family=";
PROGMEM static const char READROM_CRC[]="readrom: crc=";
PROGMEM static const char CONF[]="conf=";

static void ds18b20_test_callback(struct ds18b20_conf *conf) {
    console_puts_P(TEMP);
    console_putd10(conf->mem->temperature.degree);
    console_putc('.');
    console_putd10(conf->mem->temperature.fract);
    console_next_line();
}

void ds18b20_test() {
    struct onewire_dev devs[5], *dev;
    unsigned char num_devs;
    int i;
    num_devs=onewire_search_rom(devs, 5);
    console_put_msg(NUM_DEVS,num_devs);
    for(i=0;i<num_devs;i++) {
        console_putd(i);
        console_puts_P(CRC);
        console_putd(devs[i].crc);
        console_putc(' ');
        console_putmem(devs[i].serial, 6);
        console_put_msg(FAMILY, devs[i].family);
    }
    if(num_devs == 1) {
        dev = devs;
        memset(devs,0, sizeof(*dev));
        onewire_read_rom(dev);
        console_puts_P(READROM_CRC);
        console_putd(dev->crc);
        console_putc(' ');
        console_putmem(dev->serial, 6);
        console_put_msg(FAMILY, dev->family);
    }
    if(num_devs > 1)
        dev = devs + 1;
        
    if(num_devs > 0) {
        struct ds18b20_mem mem;
        struct ds18b20_proc proc = {
            .callback=ds18b20_test_callback,
            .conf={
                .mem=&mem,
                .rom=dev
            }
        };
        memset(&mem,0, sizeof(proc.conf.mem));
        ds18b20_read_conf(&proc.conf);
        console_puts_P(CONF);
        console_putmem(&mem, sizeof(mem));
        console_next_line();
        mem.conf.resolution=1;
        ds18b20_start_temp(&proc, 1);
        while(proc.in_process == 1)
            timer_task();

        while(1) {
            ds18b20_start_temp(&proc, 0);
            while(proc.in_process == 1)
            timer_task();
            sleep(2);
        }
        
    }
    return;
}
