#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

enum KEYCODE {
    KEY_1=0,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8
};

void keyb_init();

/* if a key was pressed*/
char kbhit();

/* return keycode or blocked until key will be pressed */
char get_keycode();

#endif /*_KEYBOARD_H_*/
