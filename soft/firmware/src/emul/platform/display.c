#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#define __USE_MISC
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdarg.h>
#include <string.h>
#include <sys/select.h>
#include <unistd.h>
#include <pthread.h>

#include <platform/display.h>
#include <platform/utils.h>
#include <utils/strconv.h>

/*hacked declarations*/
FILE *fdopen(int fildes, const char *mode);
int fsync(int fd);


static const char *dest_host="127.0.0.1";
static const unsigned short dest_port=9998;

static int sock_fd;
static int pipefd[2];
static FILE *sock_out=NULL;
static int is_stop_read_thread;
static void (*current_key_handler)(char flags)=NULL;
static pthread_t tid;

static void connection_check()
{
    if(sock_out==NULL) {
        fprintf(stderr,"Connection to Emulator is not established!\n");
        abort();
    }
}

static int getstr(int fd, char *buf, int buf_size) {
    int cnt=0;
    char *ptr=buf;
    
    while(!is_stop_read_thread && cnt<buf_size) {
       fd_set rfds;
       struct timeval tv;

       FD_ZERO(&rfds);
       FD_SET(fd, &rfds);

       tv.tv_sec=1;
       tv.tv_usec=0;
       switch(select(fd+1,&rfds,NULL,NULL,&tv)) {
           case -1:
               perror("select()");
               break;
           case 0:
               break;
           default:
               if(FD_ISSET(fd, &rfds)) {
                   read(fd,ptr,1);
                   ptr++;
                   cnt++;
               }
       }

       if(cnt>0 && *(ptr-1)=='\n') {
           *ptr=0;
           break;
       }
    }
    return cnt;
}

static void handle_responce()
{
    char buf[128];

    buf[0]=0;
    getstr(pipefd[0],buf,128);
/*     printf("Got responce: '%s'\n",buf); */
    if(strncmp(buf,"OK",2)) {
        fprintf(stderr,"Bad responce!\n");
    }
}

static void *read_thread(void *arg)
{
    char buf[128];
    int rb;

    while(!is_stop_read_thread) {
/*         printf("read_thread: start read\n"); */
        rb=getstr(sock_fd,buf,128);
        if(!rb) {
            continue;
        }
/*         printf("read_thread: read '%s'\n",buf); */
        if(!strncmp("GB",buf,2)) {
            debug_print("Key status: '%2s'\n", buf+2);
            if(current_key_handler != NULL) {
                current_key_handler(~str2byte(buf+2));
            }
        } else {
            write(pipefd[1],buf,strlen(buf));
            fsync(pipefd[1]);
        }
    }
    printf("Exit read_thread!\n");
    return NULL;
}

void emul_send_command(int wait_resp, char *fmt, ...)
{
    va_list ap;

    connection_check();
    va_start(ap, fmt);
    vfprintf(sock_out,fmt,ap);
    fflush(sock_out);
    va_end(ap);
    if(wait_resp)
        handle_responce();
}

void display_set_key_handler(void(*key_handler)(char flags))
{
    current_key_handler=key_handler;
}

void display_init()
{
    struct sockaddr_in addr;
    struct hostent *hostent=gethostbyname(dest_host);

    if(hostent==NULL) {
        fprintf(stderr,"Can't resolve host address: '%s', aborted!\n",
                dest_host);
        abort();
    }

    printf("Connecting to emulator: %s:%d\n",dest_host,dest_port);

    if((sock_fd=socket(PF_INET, SOCK_STREAM, 0))==-1) {
        fprintf(stderr,"Can't create socket, aborted!\n");
        abort();
    }
    addr.sin_family=AF_INET;
    addr.sin_port=htons(dest_port);
    memcpy(&addr.sin_addr,hostent->h_addr,sizeof(struct in_addr));

    if(connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr))==-1) {
        fprintf(stderr,"Can't connect to emulator: %s:%d, aborted!\n",
                dest_host,dest_port);
        abort();
    }
    if((sock_out=fdopen(sock_fd,"r+"))==NULL) {
        fprintf(stderr,"Can't create stream for socket!\n");
        abort();
    }
    
    is_stop_read_thread=0;
    pipe(pipefd);
    pthread_create(&tid, NULL, read_thread, NULL);

    display_set_page(0,0);
    display_set_page(1,0);
    display_set_addr(0,0);
    display_set_addr(1,0);
}

void display_set_page(char controller, char page)
{
    char str1[3],str2[3];
    emul_send_command(1,"SP%s%s\r\n",byte2str(controller,str1),byte2str(page,str2));
}

void display_set_addr(char controller, char addr)
{
    char str1[3],str2[3];
    emul_send_command(1,"SX%s%s\r\n",byte2str(controller,str1),byte2str(addr,str2));
}

void display_set_data(char controller, char data)
{
    char str1[3],str2[3];
    emul_send_command(1,"SD%s%s\r\n",byte2str(controller,str1),byte2str(data,str2));
}

char display_get_data(char controller)
{
    char buf[32],str[3];
    buf[0]=0;
    connection_check();
    emul_send_command(0,"GD%s\r\n",byte2str(controller,str));
    getstr(pipefd[0],buf,32);
    return str2byte(buf);
}
