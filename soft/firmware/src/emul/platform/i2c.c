#define _POSIX_SOURCE
#include <time.h>
#include <string.h>
#include <assert.h>

#include <rtc/rtc.h>
#include <pressure/bmp085.h>
#include <platform/utils.h>
#include <platform/i2c.h>
#include <utils/utils.h>

static struct rtc_ds1307 rtc;

static unsigned char ds1307_read(unsigned char addr, 
                       unsigned char cnt, 
                       char *buf)
{
    time_t t;
    struct tm tm;

    time(&t);
    memset(&rtc,0,sizeof(rtc));
    localtime_r(&t, &tm);
    
    rtc.secs.s10=tm.tm_sec/10;
    rtc.secs.s=tm.tm_sec%10;
    rtc.mins.m10=tm.tm_min/10;
    rtc.mins.m=tm.tm_min%10;
    rtc.hours.h10=tm.tm_hour/10;
    rtc.hours.h=tm.tm_hour%10;
    rtc.days.d=tm.tm_wday;
    rtc.date.d10=tm.tm_mday/10;
    rtc.date.d=tm.tm_mday%10;
    rtc.month.m10=tm.tm_mon/10;
    rtc.month.m=tm.tm_mon%10;
    rtc.year.y10=(tm.tm_year-100)/10;
    rtc.year.y=tm.tm_year%10;

    memcpy(buf+addr,((char *)&rtc)+addr, cnt);
    return cnt;
}

static unsigned char ds1307_write(unsigned char addr, 
                       unsigned char cnt, 
                       char *buf)
{
    debug_print("ds1307_write unimplemented, addr=%d cnt=%d\n", addr, cnt);
    return cnt;
}

static unsigned char eeprom_read(unsigned char addr, 
                       unsigned char cnt, 
                       char *buf)
{
    debug_print("eeprom_read unimplemented, addr=%d cnt=%d\n", addr, cnt);
    return cnt;
}
static unsigned char eeprom_write(unsigned char addr, 
                       unsigned char cnt, 
                       char *buf)
{
    debug_print("eeprom_write unimplemented, addr=%d cnt=%d\n", addr, cnt);
    return cnt;
}

static struct bmp085_eeprom bmp085_eeprom = {
    .ac1=408,
    .ac2=-72,
    .ac3=-14383,
    .ac4=32741,
    .ac5=32757,
    .ac6=23153,
    .b1=6190,
    .b2=4,
    .mb=32768,
    .mc=-8711,
    .md=2868
};
static unsigned char bmp085_control;
static unsigned short bmp085_temperature;
static unsigned short bmp085_pressure;


static unsigned char bmp085_read(unsigned char addr, 
                                 unsigned char cnt, 
                                 char *buf) {
    switch(addr) {
        case BMP085_EEPROM_START:
            assert(cnt==sizeof(struct bmp085_eeprom));
            memcpy(buf, &bmp085_eeprom, cnt);
            return cnt;
        case BMP085_REGISTER_RESULT:
            switch(bmp085_control) {
                case BMP085_START_TEMPERATURE:
                    memcpy(buf, &bmp085_temperature, cnt);
                    return cnt;
                case BMP085_START_PRESSURE_ULP:
                case BMP085_START_PRESSURE_ST:
                case BMP085_START_PRESSURE_HI:
                case BMP085_START_PRESSURE_UHI:
                    memcpy(buf, &bmp085_pressure, cnt);
                    return cnt;
                default:
                    debug_print("i2c: unexpected bmp085_control: %d cnt=%d\n", bmp085_control, cnt);
                    return -1;
            }
            break;
        default:
            debug_print("i2c: bcm085 read: unexpected addr: %d cnt=%d\n", addr, cnt);
            return -1;
    }
    return -1;
}

static unsigned char bmp085_write(unsigned char addr, 
                                 unsigned char cnt, 
                                 char *buf) {
    switch(addr) {
        case BMP085_REGISTER_CONTROL:
            memcpy(&bmp085_control, buf, cnt);
            return cnt;
        default:
            debug_print("i2c: bcm085 write: unexpected addr: %d cnt=%d\n", addr, cnt);
            return -1;
    }
}

void i2c_init()
{
    debug_puts("i2c_init()\n");
    bmp085_eeprom.ac1=swap_short(bmp085_eeprom.ac1);
    bmp085_eeprom.ac2=swap_short(bmp085_eeprom.ac2);
    bmp085_eeprom.ac3=swap_short(bmp085_eeprom.ac3);
    bmp085_eeprom.ac4=swap_ushort(bmp085_eeprom.ac4);
    bmp085_eeprom.ac5=swap_ushort(bmp085_eeprom.ac5);
    bmp085_eeprom.ac6=swap_ushort(bmp085_eeprom.ac6);
    bmp085_eeprom.b1=swap_short(bmp085_eeprom.b1);
    bmp085_eeprom.b2=swap_short(bmp085_eeprom.b2);
    bmp085_eeprom.mb=swap_ushort(bmp085_eeprom.mb);
    bmp085_eeprom.mc=swap_short(bmp085_eeprom.mc);
    bmp085_eeprom.md=swap_short(bmp085_eeprom.md);
    bmp085_temperature = swap_ushort(27898);
    bmp085_pressure = swap_ushort(23843);
}


unsigned char i2c_read(unsigned char dev_no, 
                       unsigned char addr, 
                       unsigned char cnt, 
                       char *buf)
{
    switch(dev_no) {
        case I2C_DEV_NO_DS1307:
            return ds1307_read(addr, cnt, buf);
        case I2C_DEV_NO_24C32:
            return eeprom_read(addr, cnt, buf);
        case I2C_DEV_NO_BMP085:
            return bmp085_read(addr, cnt, buf);
        default:
            return -1;
    }
}

unsigned char i2c_write(unsigned char dev_no, 
                       unsigned char addr, 
                       unsigned char cnt, 
                       char *buf)
{
    switch(dev_no) {
        case I2C_DEV_NO_DS1307:
            return ds1307_write(addr, cnt, buf);
        case I2C_DEV_NO_24C32:
            return eeprom_write(addr, cnt, buf);
        case I2C_DEV_NO_BMP085:
            return bmp085_write(addr, cnt, buf);
        default:
            return -1;
    }
}
