#ifndef _PLATFORM_DELAY_H_
#define _PLATFORM_DELAY_H_

#define _BSD_SOURCE
#include <unistd.h>

#define _delay_ms(t) usleep(t*1000)
#define _delay_us(t) usleep(t)

#endif /*_PLATFORM_DELAY_H_*/
