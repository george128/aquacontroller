#define _BSD_SOURCE
/* for usleep declaration*/
#include <unistd.h>
#include <string.h>

#include <platform/utils.h>

#include <onewire/onewire.h>
#include <onewire/ds18b20.h>
#include <platform/onewirehw.h>

#define DEB_PREFIX "1-wire: "

#define NUM_DEVS 4
static unsigned char devs_ids[][8] = 
{
    {0x01,0xD3,0xC7,0x1A,0x00,0x10,0x00,0x22},
    {0x01,0xD0,0xC7,0x1A,0x10,0x20,0x01,0x00},
    {0x01,0xD0,0xC7,0x1A,0x20,0x30,0x02,0x00},
    {0x01,0xD2,0xC7,0x1A,0x30,0x40,0x03,0x00},
};

static struct ds18b20_mem devs_mem[] = 
{
    {
        .temperature={.fract=0,.degree=0x55,.sign=0},
        .user=0,
        .conf={.zero=0,.resolution=3,.one=31},
        .reserved1=0xff,
        .reserved2=0,
        .reserved3=0x10,
        .crc=0
    },
    {
        .temperature={.fract=1,.degree=0x56,.sign=0},
        .user=0,
        .conf={.zero=0,.resolution=3,.one=31},
        .reserved1=0xff,
        .reserved2=0,
        .reserved3=0x10,
        .crc=1
    },
    {
        .temperature={.fract=2,.degree=0x57,.sign=0},
        .user=0,
        .conf={.zero=0,.resolution=3,.one=31},
        .reserved1=0xff,
        .reserved2=0,
        .reserved3=0x10,
        .crc=2
    },
    {
        .temperature={.fract=3,.degree=0x58,.sign=0},
        .user=0,
        .conf={.zero=0,.resolution=3,.one=31},
        .reserved1=0xff,
        .reserved2=0,
        .reserved3=0x10,
        .crc=3
    },
};

static int bit_cnt_read;
static int bit_cnt_write;

static unsigned char cmd;
static unsigned char cmd_tmp;
static unsigned char cmd_write_cnt;
static unsigned char mode;
static unsigned char search_bit_no;
static unsigned char rom_mask[8];
static int rom_mask_cnt;

static char get_bit(char *prefix, unsigned char *buf, int bit_no) {
    char b = buf[bit_no/8];
    char shft = ONEWIRE_MASK(bit_no%8);
    char bit = (b & shft) ? 1 : 0;
    //debug_print(DEB_PREFIX "get_bit %s: get_bit[%d]=%02x(%02x %02x)\n",
    //            prefix, bit_no,bit, b, shft);
    return bit;
}

static char is_dev_selected(int dev_id) {
    int i;
    char res = 1;
    //debug_print(DEB_PREFIX "is_dev_selected[%d] mask_cnt=%d start\n",
    //            dev_id, rom_mask_cnt);
    for(i=0;i<rom_mask_cnt;i++) {
        if(get_bit("sel-dev", devs_ids[dev_id], i) != get_bit("sel-mask", rom_mask, i)) {
            res = 0;
            break;
        }
    }
    //debug_print(DEB_PREFIX "is_dev_selected[%d]=%d\n",
    //            dev_id, res);

    return res;
}

static char get_rom_bit(int bit_cnt, char *dev_sel_map) {
    int dev_id;
    char bit_sum = 0;
    for(dev_id = 0; dev_id < NUM_DEVS; dev_id++) {
        unsigned char *dev = devs_ids[dev_id];
        if(dev_sel_map[dev_id]) {
            if(get_bit("rom-dev", dev,bit_cnt))
                bit_sum++;
        }
    }
    //debug_print(DEB_PREFIX "get_rom_bit[%d]=%d\n",bit_cnt,bit_sum);

    return bit_sum;
}

static void write_rom_mask(char bit)
{
    char b = bit_cnt_write/8;
    char shft = ONEWIRE_MASK(bit_cnt_write%8);
    if(bit) {
        rom_mask[b] |= shft;
    }
    rom_mask_cnt = bit_cnt_write+1;
    //if(rom_mask_cnt%8==0)
    //    debug_print(DEB_PREFIX 
    //                "write %d rom_mask[%d]=%02x shft=%02x(%d)\n", 
    //                bit, b, rom_mask[b], shft, rom_mask_cnt);
}

static void process_cmd() {
    debug_print(DEB_PREFIX "process byte: %02x\n", cmd_tmp);
    bit_cnt_read = 0;
    rom_mask_cnt = 0;
    bit_cnt_write = 0;
    if(mode>=0 && mode < NUM_DEVS) {
        if(cmd == 0) {
            cmd = cmd_tmp;
            cmd_write_cnt = 0;
            debug_print(DEB_PREFIX "dev[%d] command: %02x\n", mode, cmd_tmp);
        } else {
            switch(cmd) {
                case DS18B20_WRITE_SCRATCH:
                {
                    char *mem=(char *)&devs_mem[mode].user;
                    mem[cmd_write_cnt++]=cmd_tmp;
                    if(cmd_write_cnt==3) {
                        cmd=0;
                    }
                        
                }
                    break;
                default:
                    debug_print(DEB_PREFIX "Warnong: skip byte %x cmd=%x dev=%d", cmd_tmp, cmd, mode);
            }
        }
    } else {
        mode = cmd_tmp;
    }

    switch(cmd_tmp) {
        case ONEWIRE_SEARCHROM:
            debug_puts(DEB_PREFIX "set mode: SEARCHROM\n");
            memset(rom_mask,0,8);
            search_bit_no = 0;
            break;
        case ONEWIRE_READROM:
            debug_puts(DEB_PREFIX "set mode: READROM\n");
            break;
        case ONEWIRE_MATCHROM:
            memset(rom_mask,0,8);
            debug_puts(DEB_PREFIX "set mode: MATCHROM\n");
            break;
    }
    cmd_tmp=0;
}

void onewirehw_write_bit(char bit)
{
    //debug_print(DEB_PREFIX "mode=%02x write_bit[%d]: %d\n", 
    //            mode, bit_cnt_write, bit?1:0);
    
    switch(mode) {
        case ONEWIRE_SEARCHROM:
        case ONEWIRE_MATCHROM:
            write_rom_mask(bit);
            break;
        default:
            //debug_print(DEB_PREFIX "write_bit collect cmd[%d]\n", bit_cnt_write);
            if(bit)
                cmd_tmp|=1<<bit_cnt_write;
    }
    bit_cnt_write++;
    switch(mode) {
        case ONEWIRE_MATCHROM:
            if(bit_cnt_write == 64) {
                mode = -1;
                cmd = 0;
                cmd_tmp = 0;
                for(char i=0; i< NUM_DEVS; i++) {
                    if(is_dev_selected(i)) {
                        mode = i;
                        break;
                    }
                }
                debug_print(DEB_PREFIX "MATCHROM: selected dev %d\n", mode);
                bit_cnt_write = 0;
            }
            break;
        case ONEWIRE_SEARCHROM:
            if(bit_cnt_write == 64) {
                bit_cnt_write = 0;
                mode = -1;
            }
            break;
        default:
            if(bit_cnt_write == 8)
                process_cmd();
            
    }
}

static char searchrom_bit_read() {
    char bit_sum = 0;
    char ret;
    if(search_bit_no)
        bit_cnt_read--;
    {
        char dev_sel_map[NUM_DEVS];
        char num_dev_sel = 0;
        for(char i=0; i< NUM_DEVS; i++) {
            dev_sel_map[i] = is_dev_selected(i);
            num_dev_sel += dev_sel_map[i];
        }
        bit_sum = get_rom_bit(bit_cnt_read, dev_sel_map);
        //debug_print(DEB_PREFIX "SEARCHROM read_bit[%d]: selected %d bit_sum=%d\n", 
        //            bit_cnt_read, num_dev_sel, bit_sum);
            
        if(num_dev_sel == 0)
            ret = search_bit_no ? 1: 0;
        if(bit_sum == 0)
            ret = 0;
        else if(bit_sum == num_dev_sel)
            ret = 1;
        else 
            ret = 2;
    }

    if(search_bit_no == 0) {
        search_bit_no = 1;
        ret = ret == 1 ? 1: 0;
    } else {
        search_bit_no = 0;
        ret = ret == 0? 1: 0;
    }
    return ret;
}

static char ds18b20_mem_bit_read() {
    char ret;
    //debug_print(DEB_PREFIX "mem_read dev[%d][%d]\n", mode, bit_cnt_read);
    ret = get_bit("ds18b20_mem",(unsigned char*)&devs_mem[mode], bit_cnt_read);
    if(bit_cnt_read==71) {
        mode=-1;
    }
    return ret;
}


char onewirehw_read_bit()
{
    char ret;
    if(mode >=0 && mode < NUM_DEVS) {
        switch(cmd) {
            case DS18B20_READ_SCRATCH:
                ret = ds18b20_mem_bit_read();
                break;
            default:
                debug_print(DEB_PREFIX "Warning: unknown cmd=%d\n",cmd);
                ret = 0;
        }
    } else {
        switch(mode) {
            case ONEWIRE_SEARCHROM:
                ret = searchrom_bit_read();
                break;
            case ONEWIRE_READROM:
                ret = get_bit("read-rom", devs_ids[0], bit_cnt_read);
                break;
            default:
                debug_print(DEB_PREFIX "Warning: unknown mode=%d\n",mode);
                ret = 0;
        }
    }
    //debug_print(DEB_PREFIX "mode=%02x read_bit[%d]: %d\n", 
    //            mode, bit_cnt_read, ret);
    bit_cnt_read++;
    return ret;
}

char onewirehw_reset()
{
    debug_puts("1-wire: reset\n");
    bit_cnt_read=bit_cnt_write=0;
    cmd=0;
    cmd_tmp=0;
    cmd_write_cnt=0;
    mode=-1;
    usleep(700);
    {
        int i;
        for(i=0;i<64;i++) {
            get_bit("test", devs_ids[0], i);
        }
    }
    return 1;
}

