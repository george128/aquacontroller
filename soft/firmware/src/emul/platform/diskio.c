#include <stdio.h>

#include <disk/diskio.h>
#include <utils/strings.h>
#include <console/console.h>
#include "utils.h"

#define FILE_IMG "sdcard.img"

/* Card type flags (CardType) */
#define CT_MMC          0x01            /* MMC ver 3 */
#define CT_SD1          0x02            /* SD ver 1 */
#define CT_SD2          0x04            /* SD ver 2 */
#define CT_SDC          (CT_SD1|CT_SD2) /* SD */
#define CT_BLOCK        0x08            /* Block addressing */

static volatile DSTATUS Stat;      /* Disk status */
static FILE *fd;

void disk_init() {
    if(!Stat) {
        Stat = STA_NOINIT;
        console_puts_P(STR_INIT_DISK);
    }
}

DSTATUS disk_initialize (BYTE drv /* Physical drive nmuber (0) */)
{
    if (drv) return STA_NOINIT;             /* Supports only single drive */
    if((fd=fopen(FILE_IMG,"r+"))==NULL) {
        return STA_NOINIT;
    }
    Stat &= ~STA_NOINIT;
    return Stat;
}

DSTATUS disk_status (BYTE drv /* Physical drive nmuber (0) */)
{
        if (drv) return STA_NOINIT;             /* Supports only single drive */
        return Stat;
}

DRESULT disk_read (
        BYTE drv,                       /* Physical drive nmuber (0) */
        BYTE *buff,                     /* Pointer to the data buffer to store read data */
        DWORD sector,           /* Start sector number (LBA) */
        BYTE count                      /* Sector count (1..255) */
)
{
    debug_print("disk_read sector=%ld count=%x\n", sector, count);
    if (drv || !count) return RES_PARERR;
    if (Stat & STA_NOINIT) return RES_NOTRDY;

    fseek(fd, sector*512, SEEK_SET);
    int rs = fread(buff, 512, count, fd);
    return rs == count? RES_OK: RES_ERROR;
}

DRESULT disk_write (
        BYTE drv,                       /* Physical drive nmuber (0) */
        const BYTE *buff,       /* Pointer to the data to be written */
        DWORD sector,           /* Start sector number (LBA) */
        BYTE count                      /* Sector count (1..255) */
)
{
    fseek(fd, sector*512, SEEK_SET);
    int rs= fwrite(buff, 512, count, fd);

    return rs == count? RES_OK: RES_ERROR;
}

DRESULT disk_ioctl (
        BYTE drv,               /* Physical drive nmuber (0) */
        BYTE ctrl,              /* Control code */
        void *buff              /* Buffer to send/receive control data */
)
{
    DRESULT res;
    char *ptr = buff;

    if (drv) return RES_PARERR;
    if (ctrl == CTRL_POWER) {
        switch (ptr[0]) {
	case 0:         /* Sub control code (POWER_OFF) */
            debug_puts("Disk: Power off\n");
            res = RES_OK;
            break;
        case 1:         /* Sub control code (POWER_GET) */
            ptr[1] = 1;
            debug_puts("Disk: Power get\n");
            res = RES_OK;
            break;
        default :
            res = RES_PARERR;
        }
    } else {
	if (Stat & STA_NOINIT) return RES_NOTRDY;
	switch (ctrl) {
	case CTRL_SYNC:
	    debug_puts("Disk: sync\n");
	    res = RES_OK;
	    break;
	case GET_SECTOR_COUNT:
	    fseek(fd, 8, SEEK_END);
	    *(DWORD*)buff = ftell(fd);
	    debug_print("Disk: sector count=%ld\n", *(DWORD*)buff);
            res = RES_OK;
	    break;
	case GET_SECTOR_SIZE :  /* Get R/W sector size (WORD) */
	    *(WORD*)buff = 512;
	    debug_puts("Disk: sector size\n");
            res = RES_OK;
            break;
	case GET_BLOCK_SIZE :   /* Get erase block size in unit of sector (DWORD) */
	    *(DWORD*)buff = 256;
	    debug_puts("Disk: block size\n");
            res = RES_OK;
            break;
	case MMC_GET_TYPE :             /* Get card type flags (1 byte) */
	    *ptr = CT_SD2;
	    debug_puts("Disk: mmc type\n");
            res = RES_OK;
            break;
	case MMC_GET_CSD :              /* Receive CSD as a data block (16 bytes) */
	    strcpy(ptr,"CARD_CSD       ");
	    debug_puts("Disk: mmc csd\n");
            res = RES_OK;
            break;
	case MMC_GET_CID :              /* Receive CID as a data block (16 bytes) */
	    strcpy(ptr,"CARD_CID       ");
	    debug_puts("Disk: mmc cid\n");
            res = RES_OK;
            break;
	case MMC_GET_OCR :              /* Receive OCR as an R3 resp (4 bytes) */
	    debug_puts("Disk: mmc ocr\n");
	    res = RES_OK;
            break;
        case MMC_GET_SDSTAT :   /* Receive SD statsu as a data block (64 bytes) */
	    debug_puts("Disk: mmc sdstat\n");
	    res = RES_OK;
            break;
	default:
            res = RES_PARERR;
	}
    }
    return res;
}

void disk_timerproc (void)
{
}
