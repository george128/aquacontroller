#include <platform/systime.h>
#include <sys/time.h>


static struct timeval start_time;

void systime_init()
{
    /*nothing to init*/
}

/*Get system time in ms*/
unsigned short get_systime()
{
    struct timeval now_time;
    long systime_cnt;

    if(!start_time.tv_sec) {
        gettimeofday(&start_time,0);
    }
    gettimeofday(&now_time,0);
    
    systime_cnt=(now_time.tv_sec-start_time.tv_sec)*1000 +
        (now_time.tv_usec-start_time.tv_usec)/1000;

    return (unsigned short)(systime_cnt&0xffff);
}
