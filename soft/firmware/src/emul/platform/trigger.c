#include <platform/trigger.h>
#include <utils/strconv.h>
#include "utils.h"

void emul_send_command(int wait_resp, char *fmt, ...);

static unsigned char trigger_state;

void trigger_init()
{
    debug_puts("Trigger init\n");
    trigger_set_all(0);
}

void trigger_set(char num, unsigned char state)
{
    char str[3];
    debug_print("trigger_state=%s\n",byte2str(trigger_state, str));
    if(state)
        trigger_state|=1<<num;
    else
        trigger_state&=~(1<<num);
    emul_send_command(1,"ST%s\r\n",byte2str(trigger_state, str));
}

void trigger_set_all(unsigned char all_state)
{
    char str[3];
    trigger_state = all_state;
    debug_print("trigger_state=%s\n",byte2str(trigger_state, str));
    emul_send_command(1,"ST%s\r\n",byte2str(trigger_state, str));
}
