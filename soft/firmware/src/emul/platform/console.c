#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#define __USE_XOPEN_EXTENDED
#include <signal.h>

#include <console/console.h>
#include <platform/utils.h>

static void tty_init()
{
    printf("tty_init\n");
}

static void tty_putc(char ch)
{
    printf("%c", ch);
}

static char tty_readc()
{
    char ch;
    if(read(1, &ch, 1)>0)
        return ch;
    return -1;
}

static char tty_input_check(char timeout_ms)
{
    fd_set rfds;
    struct timeval tv;
    int retval;

    /* Watch stdin (fd 0) to see when it has input. */
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);

    tv.tv_sec = 0;
    tv.tv_usec = ((long)timeout_ms)* 1000;

    switch((retval=select(1, &rfds, NULL, NULL, &tv))) {
        case -1:
            perror("select stdin");
            abort();
        case 0:
            break;
        default:
            return 1;
    }
    return 0;
}

static const struct console_dev console_tty =
{
    .init=tty_init,
    .putc=tty_putc,
    .readc=tty_readc,
    .input_check=tty_input_check,
};

static struct termios tty_orig;

static void tty_cleanup(int arg) {
    debug_puts("tty_cleanup\n");
    tcsetattr(0,TCSANOW,&tty_orig);
    exit(0);
}

void console_platform_init()
{
    struct termios tty_new;
    memset (&tty_orig, 0, sizeof(tty_orig));
    if (tcgetattr (0, &tty_orig) != 0) {
        debug_print("error %d from tcgetattr", errno);
        abort();
    }
    memcpy(&tty_new, &tty_orig, sizeof(tty_orig));
    tty_new.c_cc[VMIN] = 1; /*min 1 char*/
    tty_new.c_cc[VTIME] = 10; /* timeout 1 sec */
    tty_new.c_lflag &= ~(ICANON | ECHO);
    
    tcsetattr(0,TCSANOW,&tty_new);

    //register_console(&console_tty);
    register_console(&console_tty);
    sigset(SIGINT, tty_cleanup);
    sigset(SIGABRT, tty_cleanup);
    sigset(SIGTERM, tty_cleanup);
}

