#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdio.h>
#include <string.h>

#define PROGMEM

#define read_rom(a) *(a)
#define memcpy_P(src,dst,size) memcpy(src,dst,size)
#define strcpy_P(src,dst) strcpy(src,dst)
#define pgm_read_word(addr) (*(long *)addr)
#define pgm_read_byte(addr) (*(char *)addr)

#define PGM_P const char *

#define debug_print(a, ...) printf(a,## __VA_ARGS__)
#define debug_puts(a) printf(a)

#endif /*_UTIL_H_*/
