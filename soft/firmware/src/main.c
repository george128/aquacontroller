#include <graphics/graphics.h>
#include <keyboard/keyboard.h>
#include <timer/timer.h>
#include <screens/module_main.h>
#include <rtc/rtc.h>
#include <disk/ff.h>
#include <console/console.h>
#include <platform/utils.h>
#include <platform/systime.h>
#include <platform/i2c.h>
#include <platform/trigger.h>
#include <utils/strings.h>

/*void main(void) __attribute__ ((naked));*/
void ds18b20_test();
void bmp085_test();

int main(void)
{
    console_init();
    systime_init();
    graphics_init();
    keyb_init();
    trigger_init();
    i2c_init();
    rtc_init();
    f_init();
    module_init();
    console_puts_P(STR_STARTING);

    clear_screen();
    /*
    {
        bmp085_test();
        ds18b20_test();
        return 0;
    }*/
    module_push(&module_main);

    console_puts_P(STR_MAIN_LOOP);
    for(;;) {
        char ch;
        if(kbhit()) {
            module_key_press(get_keycode());
        }
        if((ch=console_read(1))>0) {
            console_puts_P(STR_MAIN_CONSOLE);
            console_putc(ch);
            console_putc('\'');
            console_putc('\n');
            if(ch >= '1' && ch <= '8')
                module_key_press(ch - '1');
        }
        timer_task();
        module_task();
    } 

    return 0;
}
