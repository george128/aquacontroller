#ifndef _RTC_H_
#define _RTC_H_

struct rtc_ds1307 {
    struct {
        unsigned int s:4;
        unsigned int s10:3;
        unsigned int halt:1;
    } __attribute__ ((packed)) secs;
    
    struct {
        unsigned int m:4;
        unsigned int m10:3;
        unsigned int unused:1;
    } __attribute__ ((packed)) mins;

    struct {
        unsigned int h:4;
        unsigned int h10:2;
        unsigned int t_12_24:1;
        unsigned int unused:1;
    } __attribute__ ((packed)) hours;
    
    struct {
        unsigned int d:3;
        unsigned int unused:5;
    } __attribute__ ((packed)) days;

    struct {
        unsigned int d:4;
        unsigned int d10:2;
        unsigned int unused:2;
    } __attribute__ ((packed)) date;

    struct {
        unsigned int m:4;
        unsigned int m10:1;
        unsigned int unused:3;
    } __attribute__ ((packed)) month;
    
    struct {
        unsigned int y:4;
        unsigned int y10:4;
    } __attribute__ ((packed)) year;
    
    struct {
        unsigned int rs:2;
        unsigned int unused2:2;
        unsigned int sqwe:1;
        unsigned int unused1:2;
        unsigned int out:1;
    } __attribute__ ((packed)) control;
    
} __attribute__ ((packed));

struct rtc {
    unsigned char year10;
    unsigned char year;
    unsigned char month10;
    unsigned char month;
    unsigned char day10;
    unsigned char day;
    unsigned char hour10;
    unsigned char hour;
    unsigned char min10;
    unsigned char min;
    unsigned char sec10;
    unsigned char sec;
};

void rtc_init();
void rtc_test();
void rtc_get(struct rtc *rtc);
void rtc_set(struct rtc *rtc);

#endif /*_RTC_H_*/
