#include <rtc/rtc.h>
#include <platform/i2c.h>
#include <platform/utils.h>
#include <console/console.h>
#include <utils/strings.h>

void rtc_init()
{
    struct rtc_ds1307 rtc_1307;

    i2c_read(I2C_DEV_NO_DS1307, 0, 8, (char *)&rtc_1307);
    if(rtc_1307.secs.halt) {
        rtc_1307.secs.halt = 0;
        i2c_write(I2C_DEV_NO_DS1307, 0, 1, (char *)&rtc_1307);
    }
}

void rtc_test()
{
    struct rtc_ds1307 rtc_1307;
    char *ptr=(char *)&rtc_1307;
    char i;

    i2c_read(I2C_DEV_NO_DS1307, 0, 8, ptr);
    console_puts_P(STR_1307);
    for(i=0;i<8;i++) {
        console_putd(ptr[i]);
        console_putc(' ');
    }
    console_putc('\n');
}


void rtc_get(struct rtc *rtc)
{
    struct rtc_ds1307 rtc_1307;

    i2c_read(I2C_DEV_NO_DS1307, 0, 7, (char *)&rtc_1307);
    rtc->year10=rtc_1307.year.y10;
    rtc->year=rtc_1307.year.y;
    rtc->month10=rtc_1307.month.m10;
    rtc->month=rtc_1307.month.m;
    rtc->day10=rtc_1307.date.d10;
    rtc->day=rtc_1307.date.d;
    rtc->hour10=rtc_1307.hours.h10;
    rtc->hour=rtc_1307.hours.h;
    rtc->min10=rtc_1307.mins.m10;
    rtc->min=rtc_1307.mins.m;
    rtc->sec10=rtc_1307.secs.s10;
    rtc->sec=rtc_1307.secs.s;
}

void rtc_set(struct rtc *rtc)
{
    struct rtc_ds1307 rtc_1307;
    rtc_1307.year.y10=rtc->year10;
    rtc_1307.year.y=rtc->year;
    rtc_1307.month.m10=rtc->month10;
    rtc_1307.month.m=rtc->month;
    rtc_1307.date.d10=rtc->day10;
    rtc_1307.date.d=rtc->day;
    rtc_1307.hours.h10=rtc->hour10;
    rtc_1307.hours.h=rtc->hour;
    rtc_1307.mins.m10=rtc->min10;
    rtc_1307.mins.m=rtc->min;
    rtc_1307.secs.s10=rtc->sec10;
    rtc_1307.secs.s=rtc->sec;
    i2c_write(I2C_DEV_NO_DS1307, 0, 7, (char *)&rtc_1307);
}
