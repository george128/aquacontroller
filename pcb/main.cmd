# Pin name action command file

# Start of element T2
ChangePinName(T2, 2, B)
ChangePinName(T2, 1, E)
ChangePinName(T2, 3, C)

# Start of element R23
ChangePinName(R23, 1, 1)
ChangePinName(R23, 2, 2)

# Start of element R22
ChangePinName(R22, 1, 1)
ChangePinName(R22, 2, 2)

# Start of element CONN20
ChangePinName(CONN20, 1, 1)
ChangePinName(CONN20, 4, 4)
ChangePinName(CONN20, 2, 2)
ChangePinName(CONN20, 3, 3)

# Start of element T1
ChangePinName(T1, 2, B)
ChangePinName(T1, 1, E)
ChangePinName(T1, 3, C)

# Start of element CONN13
ChangePinName(CONN13, 9, 9)
ChangePinName(CONN13, 10, 10)
ChangePinName(CONN13, 7, 7)
ChangePinName(CONN13, 5, 5)
ChangePinName(CONN13, 3, 3)
ChangePinName(CONN13, 1, 1)
ChangePinName(CONN13, 8, 8)
ChangePinName(CONN13, 6, 6)
ChangePinName(CONN13, 4, 4)
ChangePinName(CONN13, 2, 2)

# Start of element CONN12
ChangePinName(CONN12, 1, 1)
ChangePinName(CONN12, 4, 4)
ChangePinName(CONN12, 2, 2)
ChangePinName(CONN12, 3, 3)

# Start of element CONN11
ChangePinName(CONN11, 3, 3)
ChangePinName(CONN11, 1, 1)
ChangePinName(CONN11, 2, 2)

# Start of element C15
ChangePinName(C15, 2, 2)
ChangePinName(C15, 1, 1)

# Start of element C14
ChangePinName(C14, 2, 2)
ChangePinName(C14, 1, 1)

# Start of element R21
ChangePinName(R21, 1, 1)
ChangePinName(R21, 2, 2)

# Start of element R20
ChangePinName(R20, 1, 1)
ChangePinName(R20, 2, 2)

# Start of element R19
ChangePinName(R19, 1, 1)
ChangePinName(R19, 2, 2)

# Start of element R18
ChangePinName(R18, 1, 1)
ChangePinName(R18, 2, 2)

# Start of element R17
ChangePinName(R17, 1, 1)
ChangePinName(R17, 2, 2)

# Start of element C13
ChangePinName(C13, 2, 2)
ChangePinName(C13, 1, 1)

# Start of element R16
ChangePinName(R16, 1, 1)
ChangePinName(R16, 2, 2)

# Start of element R15
ChangePinName(R15, 1, 1)
ChangePinName(R15, 2, 2)

# Start of element R14
ChangePinName(R14, 1, 1)
ChangePinName(R14, 2, 2)

# Start of element C12
ChangePinName(C12, 2, 2)
ChangePinName(C12, 1, 1)

# Start of element R13
ChangePinName(R13, 1, 1)
ChangePinName(R13, 2, 2)

# Start of element Q3
ChangePinName(Q3, 2, 2)
ChangePinName(Q3, 1, 1)

# Start of element R12
ChangePinName(R12, 1, 1)
ChangePinName(R12, 2, 2)

# Start of element R11
ChangePinName(R11, 1, 1)
ChangePinName(R11, 2, 2)

# Start of element C11
ChangePinName(C11, 2, -)
ChangePinName(C11, 1, +)

# Start of element C10
ChangePinName(C10, 2, 2)
ChangePinName(C10, 1, 1)

# Start of element C9
ChangePinName(C9, 2, 2)
ChangePinName(C9, 1, 1)

# Start of element C8
ChangePinName(C8, 2, 2)
ChangePinName(C8, 1, 1)

# Start of element C7
ChangePinName(C7, 2, 2)
ChangePinName(C7, 1, 1)

# Start of element C6
ChangePinName(C6, 2, 2)
ChangePinName(C6, 1, 1)

# Start of element C5
ChangePinName(C5, 2, -)
ChangePinName(C5, 1, +)

# Start of element C4
ChangePinName(C4, 2, 2)
ChangePinName(C4, 1, 1)

# Start of element L1
ChangePinName(L1, 1, 1)
ChangePinName(L1, 2, 2)

# Start of element R10
ChangePinName(R10, 1, 1)
ChangePinName(R10, 2, 2)

# Start of element S1
ChangePinName(S1, 2, 2)
ChangePinName(S1, 1, 1)

# Start of element C3
ChangePinName(C3, 2, 2)
ChangePinName(C3, 1, 1)

# Start of element R9
ChangePinName(R9, 1, 1)
ChangePinName(R9, 2, 2)

# Start of element Q2
ChangePinName(Q2, 2, 2)
ChangePinName(Q2, 1, 1)

# Start of element C2
ChangePinName(C2, 2, 2)
ChangePinName(C2, 1, 1)

# Start of element C1
ChangePinName(C1, 2, 2)
ChangePinName(C1, 1, 1)

# Start of element R8
ChangePinName(R8, 1, 1)
ChangePinName(R8, 2, 2)

# Start of element R7
ChangePinName(R7, 1, 1)
ChangePinName(R7, 2, 2)

# Start of element D12
ChangePinName(D12, 2, 2)
ChangePinName(D12, 1, 1)

# Start of element D11
ChangePinName(D11, 2, 2)
ChangePinName(D11, 1, 1)

# Start of element R6
ChangePinName(R6, 1, 1)
ChangePinName(R6, 2, 2)

# Start of element D10
ChangePinName(D10, 2, 2)
ChangePinName(D10, 1, 1)

# Start of element D9
ChangePinName(D9, 2, 2)
ChangePinName(D9, 1, 1)

# Start of element CONN10
ChangePinName(CONN10, 3, 3)
ChangePinName(CONN10, 1, 1)
ChangePinName(CONN10, 2, 2)

# Start of element R5
ChangePinName(R5, 1, 1)
ChangePinName(R5, 2, 2)

# Start of element D8
ChangePinName(D8, 2, 2)
ChangePinName(D8, 1, 1)

# Start of element D7
ChangePinName(D7, 2, 2)
ChangePinName(D7, 1, 1)

# Start of element R4
ChangePinName(R4, 1, 1)
ChangePinName(R4, 2, 2)

# Start of element D6
ChangePinName(D6, 2, 2)
ChangePinName(D6, 1, 1)

# Start of element D5
ChangePinName(D5, 2, 2)
ChangePinName(D5, 1, 1)

# Start of element CONN9
ChangePinName(CONN9, 3, 3)
ChangePinName(CONN9, 1, 1)
ChangePinName(CONN9, 2, 2)

# Start of element R3
ChangePinName(R3, 1, 1)
ChangePinName(R3, 2, 2)

# Start of element D4
ChangePinName(D4, 2, 2)
ChangePinName(D4, 1, 1)

# Start of element D3
ChangePinName(D3, 2, 2)
ChangePinName(D3, 1, 1)

# Start of element CONN8
ChangePinName(CONN8, 3, 3)
ChangePinName(CONN8, 1, 1)
ChangePinName(CONN8, 2, 2)

# Start of element R2
ChangePinName(R2, 1, 1)
ChangePinName(R2, 2, 2)

# Start of element D2
ChangePinName(D2, 2, 2)
ChangePinName(D2, 1, 1)

# Start of element D1
ChangePinName(D1, 2, 2)
ChangePinName(D1, 1, 1)

# Start of element R1
ChangePinName(R1, 3, 3)
ChangePinName(R1, 2, 2)
ChangePinName(R1, 1, 1)

# Start of element Q1
ChangePinName(Q1, 2, 2)
ChangePinName(Q1, 1, 1)

# Start of element B1
ChangePinName(B1, 2, -)
ChangePinName(B1, 1, +)

# Start of element CONN7
ChangePinName(CONN7, 9, 9)
ChangePinName(CONN7, 7, 7)
ChangePinName(CONN7, 5, 5)
ChangePinName(CONN7, 3, 3)
ChangePinName(CONN7, 1, 1)
ChangePinName(CONN7, 8, 8)
ChangePinName(CONN7, 6, 6)
ChangePinName(CONN7, 4, 4)
ChangePinName(CONN7, 2, 2)

# Start of element U5
ChangePinName(U5, 15, SDA)
ChangePinName(U5, 14, SCL)
ChangePinName(U5, 3, A2)
ChangePinName(U5, 2, A1)
ChangePinName(U5, 1, A0)
ChangePinName(U5, 12, P7)
ChangePinName(U5, 11, P6)
ChangePinName(U5, 10, P5)
ChangePinName(U5, 9, P4)
ChangePinName(U5, 7, P3)
ChangePinName(U5, 6, P2)
ChangePinName(U5, 5, P1)
ChangePinName(U5, 4, P0)
ChangePinName(U5, 13, \_INT\_)
ChangePinName(U5, 16, Vcc)
ChangePinName(U5, 8, GND)

# Start of element LCD1
ChangePinName(LCD1, 20, 20)
ChangePinName(LCD1, 19, 19)
ChangePinName(LCD1, 18, 18)
ChangePinName(LCD1, 17, 17)
ChangePinName(LCD1, 16, 16)
ChangePinName(LCD1, 15, 15)
ChangePinName(LCD1, 14, 14)
ChangePinName(LCD1, 13, 13)
ChangePinName(LCD1, 12, 12)
ChangePinName(LCD1, 11, 11)
ChangePinName(LCD1, 10, 10)
ChangePinName(LCD1, 9, 9)
ChangePinName(LCD1, 8, 8)
ChangePinName(LCD1, 7, 7)
ChangePinName(LCD1, 6, 6)
ChangePinName(LCD1, 5, 5)
ChangePinName(LCD1, 4, 4)
ChangePinName(LCD1, 3, 3)
ChangePinName(LCD1, 2, 2)
ChangePinName(LCD1, 1, 1)

# Start of element CONN6
ChangePinName(CONN6, 1, 1)
ChangePinName(CONN6, 2, 2)

# Start of element CONN5
ChangePinName(CONN5, 1, 1)
ChangePinName(CONN5, 2, 2)

# Start of element CONN4
ChangePinName(CONN4, 3, 3)
ChangePinName(CONN4, 1, 1)
ChangePinName(CONN4, 2, 2)

# Start of element CONN3
ChangePinName(CONN3, 1, 1)
ChangePinName(CONN3, 2, 2)

# Start of element CONN2
ChangePinName(CONN2, 1, VCC)
ChangePinName(CONN2, 2, D-)
ChangePinName(CONN2, 3, D+)
ChangePinName(CONN2, 4, GND)

# Start of element CONN1
ChangePinName(CONN1, 3, 3)
ChangePinName(CONN1, 1, 1)
ChangePinName(CONN1, 2, 2)

# Start of element U4
ChangePinName(U4, 28, XTOUT)
ChangePinName(U4, 27, XTIN)
ChangePinName(U4, 13, VCIO)
ChangePinName(U4, 26, VCC2)
ChangePinName(U4, 3, VCC1)
ChangePinName(U4, 7, USBDP)
ChangePinName(U4, 8, USBDM)
ChangePinName(U4, 12, TXLED)
ChangePinName(U4, 16, TXDEN)
ChangePinName(U4, 25, TXD)
ChangePinName(U4, 31, TEST)
ChangePinName(U4, 10, SLEEP)
ChangePinName(U4, 11, RXLED)
ChangePinName(U4, 24, RXD)
ChangePinName(U4, 23, RTS)
ChangePinName(U4, 5, RSTOUT)
ChangePinName(U4, 18, RI)
ChangePinName(U4, 4, RESET)
ChangePinName(U4, 15, PWREN)
ChangePinName(U4, 14, PWRCTL)
ChangePinName(U4, 17, GND2)
ChangePinName(U4, 9, GND1)
ChangePinName(U4, 1, EESK)
ChangePinName(U4, 2, EEDATA)
ChangePinName(U4, 32, EECS)
ChangePinName(U4, 21, DTR)
ChangePinName(U4, 20, DSR)
ChangePinName(U4, 19, DCD)
ChangePinName(U4, 22, CTS)
ChangePinName(U4, 30, AVCC)
ChangePinName(U4, 29, AGND)
ChangePinName(U4, 6, 3V3OUT)

# Start of element U3
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, 7, n/c)
ChangePinName(U3, 2, A1)
ChangePinName(U3, 6, SCL)
ChangePinName(U3, 3, A2)
ChangePinName(U3, 1, A0)
ChangePinName(U3, 5, SDA)

# Start of element U2
ChangePinName(U2, 8, VCC)
ChangePinName(U2, 7, SQW/OUT)
ChangePinName(U2, 6, SCL)
ChangePinName(U2, 5, SDA)
ChangePinName(U2, 4, GND)
ChangePinName(U2, 3, VBATTERY)
ChangePinName(U2, 2, X2)
ChangePinName(U2, 1, X1)

# Start of element U1
ChangePinName(U1, 32, AREF)
ChangePinName(U1, 12, XTAL2)
ChangePinName(U1, 13, XTAL1)
ChangePinName(U1, 31, AGND)
ChangePinName(U1, 10, VCC)
ChangePinName(U1, 9, RESET)
ChangePinName(U1, 11, GND)
ChangePinName(U1, 30, AVCC)
ChangePinName(U1, 21, "(OC2) PD7")
ChangePinName(U1, 20, "(ICP) PD6")
ChangePinName(U1, 19, "(OC1A) PD5")
ChangePinName(U1, 18, "(OC1B) PD4")
ChangePinName(U1, 17, "(INT1) PD3")
ChangePinName(U1, 16, "(INT0) PD2")
ChangePinName(U1, 15, "(TXD) PD1")
ChangePinName(U1, 14, "(RXD) PD0")
ChangePinName(U1, 29, "(TOSC2) PC7")
ChangePinName(U1, 28, "(TOSC1) PC6")
ChangePinName(U1, 27, "(TDI) PC5")
ChangePinName(U1, 26, "(TDO) PC4")
ChangePinName(U1, 25, "(TMS) PC3")
ChangePinName(U1, 24, "(TCK) PC2")
ChangePinName(U1, 23, "(SDA) PC1")
ChangePinName(U1, 22, "(SCL) PC0")
ChangePinName(U1, 8, "PB7 (SCK)")
ChangePinName(U1, 7, "PB6 (MISO)")
ChangePinName(U1, 6, "PB5 (MOSI)")
ChangePinName(U1, 5, "PB4 (SS)")
ChangePinName(U1, 4, "PB3 (OC0/AIN1)")
ChangePinName(U1, 3, "PB2 (INT2/AIN0)")
ChangePinName(U1, 2, "PB1 (T1)")
ChangePinName(U1, 1, "PB0 (XCK/T0)")
ChangePinName(U1, 33, "PA7 (ADC7)")
ChangePinName(U1, 34, "PA6 (ADC6)")
ChangePinName(U1, 35, "PA5 (ADC5)")
ChangePinName(U1, 36, "PA4 (ADC4)")
ChangePinName(U1, 37, "PA3 (ADC3)")
ChangePinName(U1, 38, "PA2 (ADC2)")
ChangePinName(U1, 39, "PA1 (ADC1)")
ChangePinName(U1, 40, "PA0 (ADC0)")
